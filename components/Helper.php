<?php

namespace app\components;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;

class Helper extends Component
{
 	public static function rp($jumlah,$null='0,00',$desimal=2)
	{
		$output = '';

		if($jumlah == null) {
			$output .= number_format(0, $desimal, ',', '.');
		} elseif(is_integer($jumlah)) {
			$output .= number_format($jumlah, $desimal, ',', '.');
		} else {
			$output .= number_format($jumlah, $desimal, ',', '.');
		}

		return $output;
	}

	public static function getTanggalSingkat($tanggal)
	{
		if($tanggal==null)
			return null;

		if($tanggal=='0000-00-00')
			return null;

		$time = strtotime($tanggal);

		return date('j',$time).' '.Helper::getBulanSingkat(date('m',$time)).' '.date('Y',$time);
	}

	public static function getTanggal($tanggal)
	{
		if($tanggal==null)
			return null;

		if($tanggal=='0000-00-00')
			return null;

		$time = strtotime($tanggal);

		return date('j',$time).' '.Helper::getBulanLengkap(date('m',$time)).' '.date('Y',$time);
	}


	public static function getHariTanggal($tanggal)
	{
		if($tanggal==null)
			return null;

		if($tanggal=='0000-00-00')
			return null;

		$time = strtotime($tanggal);

		$h = date('N',$time);

		if($h == '1') $hari = 'Senin';
		if($h == '2') $hari = 'Selasa';
		if($h == '3') $hari = 'Rabu';
		if($h == '4') $hari = 'Kamis';
		if($h == '5') $hari = 'Jumat';
		if($h == '6') $hari = 'Sabtu';
		if($h == '7') $hari = 'Minggu';

		return $hari.', '.date('j',$time).' '.Helper::getBulanLengkap(date('m',$time)).' '.date('Y',$time);
	}

	public static function getBulanSingkat($i)
	{
		$bulan = '';

		if(strlen($i)==1) $i = '0'.$i;

		if($i=='01') $bulan = 'Jan';
		if($i=='02') $bulan = 'Feb';
		if($i=='03') $bulan = 'Mar';
		if($i=='04') $bulan = 'Apr';
		if($i=='05') $bulan = 'Mei';
		if($i=='06') $bulan = 'Jun';
		if($i=='07') $bulan = 'Jul';
		if($i=='08') $bulan = 'Agt';
		if($i=='09') $bulan = 'Sep';
		if($i=='10') $bulan = 'Okt';
		if($i=='11') $bulan = 'Nov';
		if($i=='12') $bulan = 'Des';

		return $bulan;

	}

	public static function getBulanLengkap($i)
	{
		$bulan = '';

		if(strlen($i)==1) $i = '0'.$i;

		if($i=='01') $bulan = 'Januari';
		if($i=='02') $bulan = 'Februari';
		if($i=='03') $bulan = 'Maret';
		if($i=='04') $bulan = 'April';
		if($i=='05') $bulan = 'Mei';
		if($i=='06') $bulan = 'Juni';
		if($i=='07') $bulan = 'Juli';
		if($i=='08') $bulan = 'Agustus';
		if($i=='09') $bulan = 'September';
		if($i=='10') $bulan = 'Oktober';
		if($i=='11') $bulan = 'November';
		if($i=='12') $bulan = 'Desember';

		return $bulan;

	}

	public static function getWaktuWIB($waktu)
	{
		if($waktu == '')
			return null;
		else {
		$time = strtotime($waktu);

		$h = date('N',$time);

		if($h == '1') $hari = 'Senin';
		if($h == '2') $hari = 'Selasa';
		if($h == '3') $hari = 'Rabu';
		if($h == '4') $hari = 'Kamis';
		if($h == '5') $hari = 'Jumat';
		if($h == '6') $hari = 'Sabtu';
		if($h == '7') $hari = 'Minggu';


		$tgl = date('j',$time);

		$h = date('n',$time);

		if($h == '1') $bulan = 'Januari';
		if($h == '2') $bulan = 'Februari';
		if($h == '3') $bulan = 'Maret';
		if($h == '4') $bulan = 'April';
		if($h == '5') $bulan = 'Mei';
		if($h == '6') $bulan = 'Juni';
		if($h == '7') $bulan = 'Juli';
		if($h == '8') $bulan = 'Agustus';
		if($h == '9') $bulan = 'September';
		if($h == '10') $bulan = 'Oktober';
		if($h == '11') $bulan = 'November';
		if($h == '12') $bulan = 'Desember';

		$tahun  = date('Y',$time);

		$pukul = date('H:i:s',$time);

		$output = $hari.', '.$tgl.' '.$bulan.' '.$tahun.' | '.$pukul.' WIB';

		return $output;
		}

	}

	public static function getBulanByInteger($h)
	{
		$bulan = '';

		if($h == '1') $bulan = 'Januari';
		if($h == '2') $bulan = 'Februari';
		if($h == '3') $bulan = 'Maret';
		if($h == '4') $bulan = 'April';
		if($h == '5') $bulan = 'Mei';
		if($h == '6') $bulan = 'Juni';
		if($h == '7') $bulan = 'Juli';
		if($h == '8') $bulan = 'Agustus';
		if($h == '9') $bulan = 'September';
		if($h == '10') $bulan = 'Oktober';
		if($h == '11') $bulan = 'November';
		if($h == '12') $bulan = 'Desember';

		return $bulan;
	}

	public static function getWaktu($waktu,$wib=false)
	{
		$time = strtotime($waktu);

		$pukul = date('H:i',$time);

		if ($wib)
			$pukul .= " WIB";

		return $pukul;
	}

	public static function getBulanList($lengkap = true, $index = true)
	{
		$bulan = [];
		$i = 1;
		if ($index) {
			if ($lengkap) {
				while ($i <= 12) {
					if (strlen($i) == 1) $i = '0'.$i;

					$bulan[$i] = self::getBulanLengkap($i);
					$i++;
				}
			} else {
				while ($i <= 12) {
					if (strlen($i) == 1) $i = '0'.$i;

					$bulan[$i] = self::getBulanSingkat($i);
					$i++;
				}
			}
		} else {
			if ($lengkap) {
				while ($i <= 12) {
					$bulan[] = self::getBulanLengkap($i);
					$i++;
				}
			} else {
				while ($i <= 12) {
					$bulan[] = self::getBulanSingkat($i);
					$i++;
				}
			}
		}

		return $bulan;
	}

	public function getBulanListFilter()
	{
		$bulan = [];
		$i = 1;
		while ($i <= 12) {
			if (strlen($i) == 1) $i = '0'.$i;

			$bulan[$i] = self::getBulanLengkap($i);
			$i++;
		}

		return $bulan;
	}

	public function getTahun($tanggal)
	{
		$data = new \DateTime($tanggal);

		return $data->format('Y');
	}

    public static function konversiRomawi($nomor)
    {
        $table = array('M'=>1000, 'CM'=>900, 'D'=>500, 'CD'=>400, 'C'=>100, 'XC'=>90, 'L'=>50, 'XL'=>40, 'X'=>10, 'IX'=>9, 'V'=>5, 'IV'=>4, 'I'=>1);
        $return = '';
        while($nomor > 0)
        {
            foreach($table as $rom => $arb)
            {
                if($nomor >= $arb)
                {
                    $nomor -= $arb;
                    $return .= $rom;
                    break;
                }
            }
        }

        return $return;
    }

}
