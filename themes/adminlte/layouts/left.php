<?php

use yii\helpers\Html;
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <?= Html::img('images/no-photo.jpg', ['class' => 'img-circle', 'alt' => 'User Image']); ?>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->username ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Menu Navigasi', 'options' => ['class' => 'header']],
                    ['label' => 'Gerbang', 'icon' => 'home', 'url' => ['site/index']],
                    ['label' => 'Data Surat', 'icon' => 'envelope', 'url' => ['surat/index']],
                    [
                        'label' => 'Distribusi',
                        'icon' => 'arrows-alt',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Data Distribusi', 'icon' => 'share', 'url' => ['distribusi/index']],
                            ['label' => 'Distribusi Grup', 'icon' => 'circle-o-notch', 'url' => ['distribusi-grup/index']],
                        ],
                    ],
                    ['label' => 'Jabatan', 'icon' => 'university', 'url' => ['jabatan/index']],
                    ['label' => 'Pegawai', 'icon' => 'users', 'url' => ['pegawai/index']],
                    ['label' => 'User', 'icon' => 'user', 'url' => ['user/index']],
                    [
                        'label' => 'Master Data',
                        'icon' => 'wrench',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Jenis Surat', 'icon' => 'envelope', 'url' => ['surat-jenis/index']],
                            ['label' => 'Metode Surat', 'icon' => 'envelope', 'url' => ['surat-metode/index']],
                            ['label' => 'Sifat Surat', 'icon' => 'envelope', 'url' => ['surat-sifat/index']],
                            ['label' => 'Status Surat', 'icon' => 'envelope', 'url' => ['surat-status/index']],
                            ['label' => 'Eselon Jabatan', 'icon' => 'address-card', 'url' => ['jabatan-eselon/index']],
                            ['label' => 'Jenis Jabatan', 'icon' => 'user-circle', 'url' => ['jabatan-jenis/index']],
                        ],
                    ],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    ['label' => 'Logout', 'url' => ['site/logout'], 'template' => '<a href="{url}" data-method="post">{icon} {label}</a>' , 'visible' => !Yii::$app->user->isGuest],

                ],
            ]
        ) ?>

    </section>

</aside>
