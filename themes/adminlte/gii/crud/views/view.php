<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\components\Helper;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */

$this->title = $model-><?= $generator->getNameAttribute() ?>;
$this->params['breadcrumbs'][] = ['label' => <?= $generator->generateString(Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary <?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-view">
    <div class="box-header with-border">
        <h1 class="box-title">Detail <?= StringHelper::basename($generator->modelClass)." <?= " ?>Html::encode($this->title) ?></h1>
    </div>
    <div class="box-body">

    <?= "<?= " ?>DetailView::widget([
        'model' => $model,
        'attributes' => [
<?php
if (($tableSchema = $generator->getTableSchema()) === false) {
    foreach ($generator->getColumnNames() as $name) {
        echo "            '" . $name . "',\n";
    }
} else {
    foreach ($generator->getTableSchema()->columns as $column) {
        $format = $generator->generateColumnFormat($column);
        if (strpos($column->name, 'tanggal') !== false) {
                echo "            [\n".
                     "                'attribute'=>'" . $column->name . "',\n".
                     "                'value'=>function(\$data) {\n".
                     "                    return Helper::getTanggalSingkat(\$data->".$column->name.");\n".
                     "                },\n".
                     "            ],\n";
        } elseif(strpos($column->name, 'id_') !== false || strpos($column->name, '_id') !== false) {
                echo "            [\n".
                     "                'attribute'=>'" . $column->name . "',\n".
                     "                'value'=>function(\$data) {\n".
                     "                    return \$data->".$column->name.";\n".
                     "                },\n".
                     "            ],\n";
        } else
            echo "            '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
    }
}
?>
        ],
    ]) ?>

    </div>
    <div class="box-footer with-border">
        <p>
            <?= "<?= " ?>Html::a(<?= $generator->generateString('<i class="fa fa-pencil"></i> Sunting') ?>, ['update', <?= $urlParams ?>], ['class' => 'btn btn-primary btn-flat']) ?>
            <?= "<?= " ?>Html::a(<?= $generator->generateString('<i class="fa fa-arrow-left"></i> Kembali') ?>, ['index', <?= $urlParams ?>], ['class' => 'btn btn-warning btn-flat']) ?>
        </p>
    </div>

</div>
