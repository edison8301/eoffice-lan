<?php

namespace app\controllers;

use Yii;
use app\models\Surat;
use app\models\SuratSearch;
use app\models\SuratStatus;
use kartik\mpdf\Pdf;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SuratController implements the CRUD actions for Surat model.
 */
class SuratController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Surat models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SuratSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Surat model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Surat model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Surat();

        // Nanti ini dibuat dinamis berdasarkan yang login!
        $model->username_pembuat = '60010951';
        $model->id_surat_status = SuratStatus::KONSEP;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->trigger(Surat::EVENT_AFTER_CREATE_DRAFT);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Surat model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->id_surat_status = SuratStatus::KONSEP;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Surat model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Surat model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Surat the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Surat::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionPdf($id)
    {
        $model = $this->findModel($id);

        $content = $this->renderPartial('_pdf',[
            'model' => $model,
        ]);

        $marginLeft = 10;
        $marginRight = 10;
        $marginTop = 10;
        $marginBottom = 20;
        $marginHeader = 5;
        $marginFooter = 5;

        $cssInline = <<<CSS
                thead {
                    display: table-header-group;
                }

                tfoot {
                    display: table-footer-group;
                }
                .padding td, .padding th {
                    padding : 10px;
                }

                .padding tr {
                    min-height: 80px;
                }

                tr, th {
                    padding:6px;
                }

                tr th.no-border-cell, tr td.no-border-cell {
                    border-width: 0px 0px 0px 1px;
                    border-style: solid;
                    border-color: black;
                }

                .no-border {
                    width: 100%;
                }

                .no-border th, .no-border td {
                    border:none;
                }

                .border {
                    width : 100%;
                    border-collapse : collapse;
                }

                .border th, .border td {
                    border : 1px solid black;
                    border-collapse : collapse;
                }

                .bold {
                    font-weight : bold;
                }

                .t-center {
                    text-align : center;
                }

                .v-center {
                    vertical-align: :center;
                }
CSS;

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_FOLIO,
            // portrait orientation
            'orientation' => Pdf::ORIENT_LANDSCAPE,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input

            'marginLeft' => $marginLeft,
            'marginRight' => $marginRight,
            'marginTop' => $marginTop,
            'marginBottom' => $marginBottom,
            'marginHeader' => $marginHeader,
            'marginFooter' => $marginFooter,

            'content' => $content,

            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            'cssInline' => $cssInline,
             // set mPDF properties on the fly
            'options' => ['title' => 'RKA SKPD'],
             // call mPDF methods on the fly
            'methods' => []
        ]);

        return $pdf->render();
    }
}
