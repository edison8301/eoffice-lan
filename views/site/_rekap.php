<?php

use app\models\SubunitProgram;
use app\models\SubunitKegiatan;
use app\models\SubunitSubkegiatan;
use app\models\Anggaran;
use yii\helpers\Url;

?>

<div class="row"><!-- /.row1 -->
    <div class="col-md-12"><!-- /.md12 -->
        <div class="panel panel-danger">
            <div class="panel-body" style="height: 48px;">
                <span class="pull-right" style="width: 100%;"><strong><marquee>Perhatian : Diharapkan kepada SKPD agar menginput data Staf pada menu perangkat, terima kasih</marquee></strong></span>
            </div>
        </div>
    </div><!-- /.md12 -->
</div><!-- /.row1 -->

<div class="row">
    
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3><?= SubunitProgram::getJumlah() ?></h3>
                <p>Program</p>
            </div>
            <div class="icon">
                <i class="fa fa-object-ungroup"></i>
            </div>
            <a href="<?= Url::to(['subunit-program/index']); ?>" class="small-box-footer" style="text-align: left; padding-left: 5px;"> Detail <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-green">
            <div class="inner">
                <h3><?= SubunitKegiatan::getJumlah() ?></h3>
                <p>Kegiatan</p>
            </div>
            <div class="icon">
                <i class="fa fa-reorder"></i>
            </div>
            <a href="<?= Url::to(['subunit-kegiatan/index']); ?>" class="small-box-footer" style="text-align: left; padding-left: 5px;"> Detail <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
        
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3><?= SubunitSubkegiatan::getJumlah() ?></h3>
                <p>Sub Kegiatan</p>
            </div>
            <div class="icon">
                <i class="fa fa-random"></i>
            </div>
            <a href="<?= Url::to(['subunit-subkegiatan/index']); ?>" class="small-box-footer" style="text-align: left; padding-left: 5px;"> Detail <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-red">
            <div class="inner">
                <h3><?= Anggaran::getJumlah() ?></h3>
                <p>Item Kegiatan</p>
            </div>
            <div class="icon">
                <i class="fa fa-qrcode"></i>
            </div>
            <a href="<?= Url::to(['anggaran/index']); ?>" class="small-box-footer" style="text-align: left; padding-left: 5px;"> Detail <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
</div>
