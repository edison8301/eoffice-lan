<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;
use miloschuman\highcharts\Highcharts;
use app\models\Anggaran;
use app\components\Helper;
use kartik\grid\Gridview;
use app\models\AnggaranMata;


$this->title = 'Rekapitulasi Anggaran';

?>

    <div class="site-anggaran">
        <div class="row">
            <!-- /.row1 -->
            <div class="col-md-12">
                <!-- /.md12 -->
                <div class="row">
                    <!-- /.row1a -->
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-aqua"><i class="fa fa-certificate"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">ANGGARAN INDUK</span>
                                <span class="info-box-number">Rp. <?= Helper::rp(Anggaran::getJumlahPagu(null,'induk')); ?></span>
                                <span class="text-info"><small>8,386 Paket</small></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-blue"><i class="fa fa-mail-forward"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">ANGGARAN LANJUTAN</span>
                                <span class="info-box-number">Rp. 2.152.000,00</span>
                                <span class="text-info"><small>6 Paket</small></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-green"><i class="fa fa-exchange"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">ANGGARAN PERUBAHAN</span>
                                <span class="info-box-number">Rp. 0</span>
                                <span class="text-info"><small>0 Paket</small></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->

                </div>
                <!-- /.row1a -->
            </div>
            <!-- /.md12 -->
        </div>
        <!-- /.row1 -->

        <div class="row">
            <!-- /.row1 -->
            <div class="col-md-8">
                <!-- /.md8 -->

                <div class="row">
                    <!-- /.row1a -->
                    <div class="col-md-12">
                        <!-- /.md12a -->
                        <div class="box box-solid box-primary">
                            <div class="box-header">
                                <h3 class="box-title">Komposisi Anggaran</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                            <?= $this->render('_anggaran_komposisi_grafik'); ?>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.md12a -->
                </div>
                <!-- /.row1a -->

                <div class="row">
                    <!-- /.row1 -->
                    <div class="col-md-12">
                        <!-- /.md12 -->
                        <div class="row">
                            <!-- /.row1a -->
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="info-box">
                                    <span class="info-box-icon bg-green"><i class="fa fa-indent"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text"><a href="/2017/detail/lelang?id=1" target="_blank">LELANG</a></span>
                                        <span class="info-box-number">Rp. <?= number_format(Anggaran::getJumlahPaguStatus(null,'lelang')); ?></span>
                                        <span class="text-info"><small>165 Paket</small></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->

                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="info-box">
                                    <span class="info-box-icon bg-red"><i class="fa fa-outdent"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text"><a href="/2017/detail/lelang?id=0" target="_blank">NON LELANG</a></span>
                                        <span class="info-box-number">Rp. <?= number_format(Anggaran::getJumlahPaguStatus(null,'non_lelang')); ?></span>
                                        <span class="text-info"><small>8,227 Paket</small></span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                            <!-- /.col -->

                        </div>
                        <!-- /.row1a -->
                    </div>
                    <!-- /.md12 -->
                </div>
                <!-- /.row1 -->

            </div>
            <!-- /.md8 -->

            <div class="col-md-4">
                <!-- /.md4 -->
                <div class="row">
                    <!-- /.row1b -->

                    <div class="col-md-12">
                        <!-- /.md12a -->
                        <div class="info-box bg-red">
                            <span class="info-box-icon">
                                <?= Html::img('@web/images/logo-banten.png',['style'=>'width: 60px']); ?>
                            </span>
                            <div class="info-box-content">
                                <span class="info-box-text">Total Anggaran</span>
                                <span class="info-box-number">Rp. <?= number_format(Anggaran::getJumlahPaguTotal()); ?></span>
                                <div class="progress">
                                    <div class="progress-bar" style="width: 100%"></div>
                                </div>
                                <span class="progress-description">
                                    <span class="pull-right" style="width: 100%;"><marquee>Pemerintah Provinsi Banten</marquee></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <!-- /.md12a -->

                    <?php print $this->render('_rekap_per_jenis'); ?>

                </div>
                <!-- /.row1b -->
            </div>
            <!-- /.md4 -->
        </div>
        <!-- /.row1 -->

        <div class="row">
            <!-- /.row1 -->
            <?= $this->render('_anggaran_satuan_kerja',[
                'query'=>$query,
            ]); ?>
            <!-- /.md12 -->
        </div>
        <!-- /.row1 -->

    </div>