<?php

use yii\helpers\Url;

?>

    <div class="row">
        <div class="col-lg-12">
            <div class="box box-success">
                <div class="box-header">
                  <i class="fa fa-bar-chart"></i>
                  <h3 class="box-title">Pejabat Pelaksana Teknis Kegiatan</h3>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover small">
                            <thead>
                                <tr align="center" valign="middle">
                                    <td width="3%" rowspan="2" style="vertical-align: middle;">No</td>
                                    <td colspan="2" rowspan="2" style="vertical-align: middle;">PPTK</td>
                                    <td width="7%" rowspan="2" style="vertical-align: middle;">Aktivitas</td>
                                    <td colspan="12" style="vertical-align: middle;">Bulan</td>
                                </tr>
                                <tr align="center" valign="middle">
                                    <td width="5%">Jan</td>
                                    <td width="5%">Feb</td>
                                    <td width="5%">Mar</td>
                                    <td width="5%">Apr</td>
                                    <td width="5%">Mei</td>
                                    <td width="5%">Jun</td>
                                    <td width="5%">Jul</td>
                                    <td width="5%">Agu</td>
                                    <td width="5%">Sep</td>
                                    <td width="5%">Okt</td>
                                    <td width="5%">Nov</td>
                                    <td width="5%">Des</td>
                                </tr>
                            </thead>
                            <tbody>                              
                                <tr align="center" valign="middle">
                                    <td rowspan="4" style="vertical-align: middle;">1</td>
                                    <td class="box box-solid" rowspan="4" style="vertical-align: middle; width: 24%;">
                                        <span>
                                        <img class="img-thumbnail img-responsive" src="images/no-photo.jpg" alt="" style="height:60px; width:50px">                                        </span>
                                        <br>
                                        <a href="<?= Url::to(['site/realisasi-pptk','id'=>1]); ?>">
                                        <span class="text-danger"><strong>Minarni T. Podungge,ST</strong></span><br>
                                        <span>Kasie aneka energi baru dan terbarukan</span></a><br>
                                        <span class="text-info">Pagu : Rp. </span>
                                        <span class="text-danger">208.750.000</span><br>
                                        <span class="text-info">Realisasi : Rp.  </span>
                                        <span class="text-danger">107.712.400</span>
                                    </td>
                                    <td width="6%" rowspan="2" style="vertical-align: middle;">Target</td>
                                    <td>Fisik</td>
                                    <td>8,86</td>
                                    <td>43,66</td>
                                    <td>46,47</td>
                                    <td>51,57</td>
                                    <td>63,95</td>
                                    <td>65,72</td>
                                    <td>67,97</td>
                                    <td>71,54</td>
                                    <td>86,20</td>
                                    <td>91,31</td>
                                    <td>92,61</td>
                                    <td>100,00</td>
                                </tr>
                                <tr align="center" valign="middle">
                                    <td>Keuangan</td>
                                    <td>7,51</td>
                                    <td>27,75</td>
                                    <td>35,94</td>
                                    <td>43,36</td>
                                    <td>51,50</td>
                                    <td>57,73</td>
                                    <td>59,14</td>
                                    <td>72,28</td>
                                    <td>81,28</td>
                                    <td>85,79</td>
                                    <td>90,87</td>
                                    <td>100,00</td>
                                </tr>
                                <tr align="center" valign="middle">
                                    <td rowspan="2" style="vertical-align: middle;">Realisasi</td>
                                    <td>Fisik</td>

                                    <td style="background-color: #2C4070; color: #FFF;  border-radius: 1px;">12,10</td>                                    
                                    <td style="background-color: #2C4070; color: #FFF;  border-radius: 1px;">43,97</td>                                    
                                    <td style="background-color: #2C4070; color: #FFF;  border-radius: 1px;">46,65</td>                                    
                                    <td style="background-color: #2C4070; color: #FFF;  border-radius: 1px;">59,32</td>                                    
                                    <td style="background-color: #2C4070; color: #FFF;  border-radius: 1px;">64,52</td>                                    
                                    <td style="background-color:green; color: #FFF;  border-radius: 1px;">64,52</td>                                    
                                    <td style="background-color:white;"></td>                                    
                                    <td style="background-color:white;"></td>                                    
                                    <td style="background-color:white;"></td>                                    
                                    <td style="background-color:white;"></td>                                    
                                    <td style="background-color:white;"></td>                                    
                                    <td style="background-color:white;"></td>                                    
                                </tr>
                                <tr align="center" valign="middle">
                                    <td>Keuangan</td>
                                    
                                    <td style="background-color: #2C4070; color: #FFF;  border-radius: 1px;">9,17</td>                                    
                                    <td style="background-color: #2C4070; color: #FFF;  border-radius: 1px;">36,55</td>                                    
                                    <td style="background-color: #2C4070; color: #FFF;  border-radius: 1px;">39,19</td>                                    
                                    <td style="background-color: #2C4070; color: #FFF;  border-radius: 1px;">51,60</td>                                    
                                    <td style="background-color: #2C4070; color: #FFF;  border-radius: 1px;">51,60</td>                                    
                                    <td style="background-color:red; color: #FFF;  border-radius: 1px;">51,60</td>                                    
                                    <td style="background-color:white;"></td>                                    
                                    <td style="background-color:white;"></td>                                    
                                    <td style="background-color:white;"></td>                                    
                                    <td style="background-color:white;"></td>                                    
                                    <td style="background-color:white;"></td>                                    
                                    <td style="background-color:white;"></td>                                
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="box-footer" style="text-align: right; padding-right: 15px;">
                </div>
                
              </div>
        </div>
    </div>
    