<?php

use yii\helpers\Html;
use miloschuman\highcharts\Highcharts;
use app\components\Helper;
use kartik\grid\GridView;
use app\models\Anggaran;



?>

<div class="panel panel-danger">
                <?= GridView::widget([
                    'dataProvider'=> $dataProviderKpa,
                    'columns' => [
                        [
                        'class' => 'yii\grid\SerialColumn',
                        'header' => 'No',
                        'headerOptions'=>['style'=>'text-align:center;width:20px;'],
                        'contentOptions'=>['style'=>'text-align:center;width:20px;']
                        ],
                        [
                            'header' => 'Foto',
                            'format' => 'raw',
                            'value' => function($data) {
                                return $data->pegawai->getFoto();
                            },
                            'headerOptions'=>['style'=>'text-align:center;'],
                            'contentOptions'=>['style'=>'text-align:center;']
                        ],
                        [
                            'header' => 'Nama',
                            'format' => 'raw',
                            'value' => function($data) {
                                return Html::a($data->pegawai->nama, ['kpa/realisasi', 'id' => $data->id]) . '<div class="text-danger"> <small> '. $data->pegawai->jabatan .' </small> </div>';
                            }
                        ],
                        [
                            'header' => 'Anggaran',
                            'value' => function($data) {
                                return Helper::rp($data->getJumlahAnggaran());
                            }
                        ],
                        [
                            'header' => 'Deviasi',
                            'value' => function($data) {
                                return $data->getJumlahDeviasi();
                            }
                        ]
                    ],
                    'layout' => '
                        <div class="panel-heading">
                            <div class="pull-right">
                                {summary}
                            </div>
                            <h3 class="panel-title">KPA Kinerja Terendah</h3>
                        </div>
                        
                            {items}
                        
                        <div class="panel-footer">
                            {pager}
                        </div>',
                    'responsive' => true,
                    'hover' => true
                ]); ?>
        </div>