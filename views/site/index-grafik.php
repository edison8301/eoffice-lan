<?php

use miloschuman\highcharts\Highcharts;
use app\components\Helper;
use app\models\Anggaran;
?>


<div class="panel panel-default">
    <?= Highcharts::widget([
       'options' => [
            'credits' => false,
            'chart' => [
                'type' => 'spline',
                'height' => 361,
            ],
            'title' => ['text' => 'Realisasi Fisik dan Keuangan'],
            'subtitle' => ['text' => 'Tahun Anggaran 2017'],
            'xAxis' => [
                'categories' => Helper::getBulanList(false, false),
            ],
            'yAxis' => [
                'title' => ['text' => '%'],
                'max' => 100,
                'mix' => 0,
            ],
            'plotOptions' => [
                'areaSpline' => [
                    'fillOpacity' => 0.5,
                ],
            ],
            'series' => [
                ['name' => 'Target Fisik', 'data' => Anggaran::getAllTargetFisikList()],
                ['name' => 'Realisasi Fisik', 'data' => Anggaran::getAllRealisasiFisikList()],
                ['name' => 'Target Keuangan', 'data' => Anggaran::getAllTargetKeuanganList()],
                ['name' => 'Realisasi Keuangan', 'data' => Anggaran::getAllRealisasiKeuanganList()],
            ]
        ]
    ]); ?>
</div>