<?php

use yii\helpers\Html;
use miloschuman\highcharts\Highcharts;
use app\components\Helper;
use kartik\grid\GridView;
use app\models\Anggaran;
use app\models\Subunit;

?>


<div class="panel panel-default">
    <?php
echo Highcharts::widget([
    'options' => [
        'title' => ['text' => 'BELANJA LANGSUNG'],
        'exporting' => ['enabled' => true],
        'plotOptions' => [
            'pie' => [
                'cursor' => 'pointer',
            ],
        ],
        'series' => [
            [ // new opening bracket
                'type' => 'pie',
                'name' => 'Organisasi',
                'data' => [
                    ['Dinas Pendidikan, <br> Kebudayaan, Pemuda <br> dan Olahraga', 75],
                    ['Dinas Kesehatan', 25],
                    ['Dinas Pekerjaan umum <br> dan penataan Ruang', 25],
                ],
            ] // new closing bracket
        ],
    ],
]);

     ?>
</div>