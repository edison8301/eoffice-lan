<div class="row">
        <div class="col-lg-12">
            <div class="box box-success">
                <div class="box-header">
                  <i class="fa fa-bar-chart"></i>
                  <h3 class="box-title">SUB KEGIATAN</h3>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover small">
                            <thead>
                                <tr align="center" valign="middle">
                                    <td width="3%" rowspan="2" style="vertical-align: middle;">No</td>
                                    <td colspan="2" rowspan="2" style="vertical-align: middle;">Sub Kegiatan</td>
                                    <td width="7%" rowspan="2" style="vertical-align: middle;">Aktivitas</td>
                                    <td colspan="12" style="vertical-align: middle;">Bulan</td>
                                </tr>
                                <tr align="center" valign="middle">
                                    <td width="5%">Jan</td>
                                    <td width="5%">Feb</td>
                                    <td width="5%">Mar</td>
                                    <td width="5%">Apr</td>
                                    <td width="5%">Mei</td>
                                    <td width="5%">Jun</td>
                                    <td width="5%">Jul</td>
                                    <td width="5%">Agu</td>
                                    <td width="5%">Sep</td>
                                    <td width="5%">Okt</td>
                                    <td width="5%">Nov</td>
                                    <td width="5%">Des</td>
                                </tr>
                            </thead>
                            <tbody>
                                
                                                                
                                <tr align="center" valign="middle">
                                    <td rowspan="4" style="vertical-align: middle;">1</td>
                                    <td class="box box-solid" rowspan="4" style="vertical-align: middle; width: 24%;">
                                        
                                        <a href="/2017/site/realisasisubkegiatan?id=02224">
                                        <span class="text-danger">02224</span><br><span><strong>Pendampingan pembangunan PLTMH di Kabupaten Gorontalo</strong></span></a><br>
                                        <span class="text-info">Pagu : Rp. </span>
                                        <span class="text-danger">53.500.000</span><br>
                                        <span class="text-info">Realisasi : Rp.  </span>
                                        <span class="text-danger">33.950.000</span>
                                    </td>
                                    <td width="6%" rowspan="2" style="vertical-align: middle;">Target</td>
                                    <td>Fisik</td>
                                    <td>14,69</td>
                                    <td>39,97</td>
                                    <td>50,90</td>
                                    <td>50,90</td>
                                    <td>59,30</td>
                                    <td>65,59</td>
                                    <td>67,25</td>
                                    <td>67,25</td>
                                    <td>81,96</td>
                                    <td>100,00</td>
                                    <td>100,00</td>
                                    <td>100,00</td>
                                </tr>
                                <tr align="center" valign="middle">
                                    <td>Keuangan</td>
                                    <td>10,00</td>
                                    <td>20,00</td>
                                    <td>35,00</td>
                                    <td>40,20</td>
                                    <td>43,30</td>
                                    <td>67,00</td>
                                    <td>67,00</td>
                                    <td>81,00</td>
                                    <td>93,00</td>
                                    <td>93,00</td>
                                    <td>100,00</td>
                                    <td>100,00</td>
                                </tr>
                                <tr align="center" valign="middle">
                                    <td rowspan="2" style="vertical-align: middle;">Realisasi</td>
                                    <td>Fisik</td>

                                    <td style="background-color:yellow; color: #000;  border-radius: 1px;">9,81</td>                                    
                                    <td style="background-color: #2C4070; color: #FFF;  border-radius: 1px;">58,18</td>                                    
                                    <td style="background-color: #2C4070; color: #FFF;  border-radius: 1px;">58,18</td>                                    
                                    <td style="background-color: #2C4070; color: #FFF;  border-radius: 1px;">66,38</td>                                    
                                    <td style="background-color: #2C4070; color: #FFF;  border-radius: 1px;">70,47</td>                                    
                                    <td style="background-color: #2C4070; color: #FFF;  border-radius: 1px;">70,47</td>                                    
                                    <td style="background-color:white;"></td>                                    
                                    <td style="background-color:white;"></td>                                    
                                    <td style="background-color:white;"></td>                                    
                                    <td style="background-color:white;"></td>                                    
                                    <td style="background-color:white;"></td>                                    
                                    <td style="background-color:white;"></td>                                    
                                </tr>
                                <tr align="center" valign="middle">
                                    <td>Keuangan</td>
                                    
                                    <td style="background-color:green; color: #FFF;  border-radius: 1px;">9,48</td>                                    
                                    <td style="background-color: #2C4070; color: #FFF;  border-radius: 1px;">55,14</td>                                    
                                    <td style="background-color: #2C4070; color: #FFF;  border-radius: 1px;">55,14</td>                                    
                                    <td style="background-color: #2C4070; color: #FFF;  border-radius: 1px;">63,46</td>                                    
                                    <td style="background-color: #2C4070; color: #FFF;  border-radius: 1px;">63,46</td>                                    
                                    <td style="background-color:yellow; color: #000;  border-radius: 1px;">63,46</td>                                    
                                    <td style="background-color:white;"></td>                                    
                                    <td style="background-color:white;"></td>                                    
                                    <td style="background-color:white;"></td>                                    
                                    <td style="background-color:white;"></td>                                    
                                    <td style="background-color:white;"></td>                                    
                                    <td style="background-color:white;"></td>                                </tr>
                                                                
                               
                                      
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="box-footer" style="text-align: right; padding-right: 15px;">
                </div>
                
              </div>
        </div>
    </div>