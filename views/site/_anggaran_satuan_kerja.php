<?php
use app\models\Anggaran;
use app\components\Helper;
use yii\helpers\Url;
?>

<div class="col-md-12">
    <!-- /.md8 -->
    <div id="w1" class="grid-view hide-resize" data-krajee-grid="kvGridInit_7c17a5ce">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="pull-right">
                    <div class="summary">Total <b>33</b> items.</div>
                </div>
                <h3 class="panel-title">Anggaran Satuan Kerja</h3>
                <div class="clearfix"></div>
            </div>
            <div style="padding: 10px" class="kv-panel-before">
                <div class="pull-right">
                    <div class="btn-toolbar kv-grid-toolbar" role="toolbar">
                        <div class="btn-group"><a class="btn btn-default" href="/2017/site/index" title="Reset"><i class="glyphicon glyphicon-repeat"></i></a></div>
                        <div class="btn-group">
                            <button id="w3" class="btn btn-default dropdown-toggle" title="Export" data-toggle="dropdown"><i class="fa fa-share-square-o"></i> <span class="caret"></span></button>
                            <ul id="w4" class="dropdown-menu dropdown-menu-right">
                                <li role="presentation" class="dropdown-header">Export Page Data</li>
                                <li title="Hyper Text Markup Language"><a class="export-html" href="#" data-mime="text/html" data-hash="834e19367edcaa315ba3e81a07ec7c98ae4d18d35ec20b6e13166b08dbc9e401grid-exporttext/htmlutf-81{&quot;cssFile&quot;:&quot;https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css&quot;}" tabindex="-1"><i class="text-info fa fa-file-text"></i> HTML</a></li>
                                <li title="Comma Separated Values"><a class="export-csv" href="#" data-mime="application/csv" data-hash="0d32a1ca77aa63adcf4ba1b3125355ac8bb72ff3c43a0447bdfe9015d1459b84grid-exportapplication/csvutf-81{&quot;colDelimiter&quot;:&quot;,&quot;,&quot;rowDelimiter&quot;:&quot;\r\n&quot;}" tabindex="-1"><i class="text-primary fa fa-file-code-o"></i> CSV</a></li>
                                <li title="Tab Delimited Text"><a class="export-txt" href="#" data-mime="text/plain" data-hash="fb507f50c878c143a199622539a262dd703ad3fa3af9ad591ca156d2f0757320grid-exporttext/plainutf-81{&quot;colDelimiter&quot;:&quot;\t&quot;,&quot;rowDelimiter&quot;:&quot;\r\n&quot;}" tabindex="-1"><i class="text-muted fa fa-file-text-o"></i> Text</a></li>
                                <li title="Microsoft Excel 95+"><a class="export-xls" href="#" data-mime="application/vnd.ms-excel" data-hash="e2bfd9cbfc144e348095baa0e5857d308820cab22bbbb3b3d4bfe963355a89f6grid-exportapplication/vnd.ms-excelutf-81{&quot;worksheet&quot;:&quot;ExportWorksheet&quot;,&quot;cssFile&quot;:&quot;&quot;}" tabindex="-1"><i class="text-success fa fa-file-excel-o"></i> Excel</a></li>
                                <li title="JavaScript Object Notation"><a class="export-json" href="#" data-mime="application/json" data-hash="554429c372defc3e3f810f3803acdd074a7e3106f073055cf388c01790543eb2grid-exportapplication/jsonutf-81{&quot;colHeads&quot;:[],&quot;slugColHeads&quot;:false,&quot;indentSpace&quot;:4}" tabindex="-1"><i class="text-warning fa fa-file-code-o"></i> JSON</a></li>
                            </ul>
                        </div>
                        <div class="btn-group"><a id="w1-togdata-page" class="btn btn-default" href="/2017/site/anggaran?_toga654c069=all" title="Show all data"><i class="glyphicon glyphicon-resize-full"></i> All</a></div>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
            <div style="padding: 10px" id="w1-container" class="table-responsive kv-grid-container" style="overflow: auto">
                <table class="kv-grid-table table table-hover table-bordered table-striped table-condensed kv-table-wrap">

                    <thead>
                        <tr class="kartik-sheet-style">
                            <th class="kv-align-center kv-align-middle" style="width: 5.15%;" data-col-seq="0">No</th>
                            <th class="kv-align-center kv-align-middle" style="width: 10.12%;" data-col-seq="1"><a class="asc" href="#" data-sort="-kode">Kode</a></th>
                            <th class="kv-align-left kv-align-middle" style="width: 57.96%;" data-col-seq="2"><a href="#" data-sort="nama">Sub Organisasi</a></th>
                            <th class="kartik-sheet-style kv-align-right kv-align-middle" style="width: 16.93%;" data-col-seq="3"><a href="#" data-sort="total">Anggaran</a></th>
                            <th class="kv-align-center kv-align-middle skip-export" style="width: 9.75%;" data-col-seq="4">#</th>
                        </tr>

                    </thead>
                    <tbody>
                        <?php $i=1; foreach ($query as $data ) { ?>
                        <tr data-key="[]">
                            <td style="width:5%;" data-col-seq="0"><?= $i; ?></td>
                            <td><?= $data->getKode()?></td>
                            <td><?= $data->keterangan?></td>
                            <td style="text-align: right"><?= Helper::rp($data->getJumlahPagu()); ?></td>
                            <td style="text-align: center"> <a class="btn btn-primary" href="<?= Url::to(['site/index']); ?>">Detail</a></td>
                        </tr>
                        <?php $i++; } ?>
                    </tbody>

                    <tbody class="kv-page-summary-container">
                        <tr class="kv-page-summary warning">
                            <td class="kv-align-center kv-align-middle" style="width:5%;">&nbsp;</td>
                            <td class="kv-align-center kv-align-middle" style="width:10%;">&nbsp;</td>
                            <td class="kv-align-left kv-align-middle" style="width:58%;">Total</td>
                            <td class="kv-align-right kv-align-middle" style="width:17%; text-align: right"><?= Helper::rp(Anggaran::getJumlahPaguTotal()); ?></td>
                            <td class="kv-align-center kv-align-middle skip-export" style="width:10%;">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="kv-panel-after"></div>
            <div class="panel-footer">
                <div class="kv-panel-pager">
                    <div>&nbsp;</div>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>