<?php
use app\models\AnggaranMata; 
use app\models\AnggaranSumber; 
use app\models\Lokasi; 
use app\models\AnggaranJenisPengadaan; 
use app\models\BelanjaKategori; 
use app\models\BelanjaJenis; 


?>
<div class="col-md-12">
    <!-- /.md12g -->
    <div class="box box-default collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title">Mata Anggaran</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="table-responsive">
                <table class="table no-margin">
                    <tbody>
                    <?php foreach (AnggaranMata::findAllMataAnggaran() as $data) { ?>
                        <tr>
                            <td><a href="#" target="_blank"><?= $data->nama; ?></a></td>
                            <td style="width: 80px; text-align: center;"><?= number_format($data->getJumlahPaket()); ?>
                                <br>Paket
                            </td>
                            <td style="text-align: right;">Rp. <?= number_format($data->getJumlahPagu()); ?>
                                <br><span class="text-danger">%</span>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>
<!-- /.md12g -->

<div class="col-md-12">
    <!-- /.md12b -->
    <div class="box box-default collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title">Sumber Dana</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="table-responsive">
                <!-- /.table-responsive -->
                <table class="table no-margin">
                    <tbody>
                    <?php foreach (AnggaranSumber::findAllAnggaranSumber() as $data) { ?>
                        <tr>
                            <td><a href="#" target="_blank"><?= $data->nama; ?></a></td>
                            <td style="width: 80px; text-align: center;"><?= number_format($data->getJumlahPaket()); ?>
                                <br>Paket
                            </td>
                            <td style="text-align: right;">Rp. <?= number_format($data->getJumlahPagu()); ?>
                                <br><span class="text-danger">%</span>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>
<!-- /.md12b -->

<div class="col-md-12">
    <!-- /.md12c -->
    <div class="box box-default collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title">Lokasi Kegiatan</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="table-responsive">
                <table class="table no-margin">
                    <tbody>
                    <?php foreach (Lokasi::findAllLokasi() as $data) { ?>
                        <tr>
                            <td><a href="#" target="_blank"><?= $data->nama; ?></a></td>
                            <td style="width: 80px; text-align: center;"><?= number_format($data->getJumlahPaket()); ?>
                                <br>Paket
                            </td>
                            <td style="text-align: right;">Rp. <?= number_format($data->getJumlahPagu()); ?>
                                <br><span class="text-danger">%</span>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>
<!-- /.md12c -->

<div class="col-md-12">
    <!-- /.md12c -->
    <div class="box box-default collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title">Jenis Pengadaan</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="table-responsive">
                <table class="table no-margin">
                    <tbody>
                    <?php foreach (AnggaranJenisPengadaan::findAllPengadaan() as $data) { ?>
                        <tr>
                            <td><a href="#" target="_blank"><?= $data->nama; ?></a></td>
                            <td style="width: 80px; text-align: center;"><?= number_format($data->getJumlahPaket()); ?>
                                <br>Paket
                            </td>
                            <td style="text-align: right;">Rp. <?= number_format($data->getJumlahPagu()); ?>
                                <br><span class="text-danger">%</span>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>
<!-- /.md12e -->

<div class="col-md-12">
    <!-- /.md12f -->
    <div class="box box-default collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title">Metode Pengadaan</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="table-responsive">
                <table class="table no-margin">
                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>
<!-- /.md12f -->

<div class="col-md-12">
    <!-- /.md12c -->
    <div class="box box-default collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title">Kategori Belanja</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="table-responsive">
                <table class="table no-margin">
                    <tbody>
                    <?php foreach (BelanjaKategori::findAllBelanjaKategori() as $data) { ?>
                        <tr>
                            <td><a href="#" target="_blank"><?= $data->nama; ?></a></td>
                            <td style="width: 80px; text-align: center;"><?= number_format($data->getJumlahPaket()); ?>
                                <br>Paket
                            </td>
                            <td style="text-align: right;">Rp. <?= number_format($data->getJumlahPagu()); ?>
                                <br><span class="text-danger">%</span>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>

<div class="col-md-12">
    <!-- /.md12c -->
    <div class="box box-default collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title">Kategori Belanja</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="table-responsive">
                <table class="table no-margin">
                    <tbody>
                    <?php foreach (BelanjaJenis::findAllBelanjaJenis() as $data) { ?>
                        <tr>
                            <td><a href="#" target="_blank"><?= $data->nama; ?></a></td>
                            <td style="width: 80px; text-align: center;"><?= number_format($data->getJumlahPaket()); ?>
                                <br>Paket
                            </td>
                            <td style="text-align: right;">Rp. <?= number_format($data->getJumlahPagu()); ?>
                                <br><span class="text-danger">%</span>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>
