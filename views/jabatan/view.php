<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\components\Helper;

/* @var $this yii\web\View */
/* @var $model app\models\Jabatan */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Jabatan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary jabatan-view">
    <div class="box-header with-border">
        <h1 class="box-title">Detail Jabatan <?= Html::encode($this->title) ?></h1>
    </div>
    <div class="box-body">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute'=>'id_induk',
                'value'=>function($data) {
                    return $data->id_induk;
                },
            ],
            'nama',
            'singkatan',
            [
                'attribute'=>'id_jabatan_jenis',
                'value'=>function($data) {
                    return $data->id_jabatan_jenis;
                },
            ],
            [
                'attribute'=>'id_jabatan_eselon',
                'value'=>function($data) {
                    return $data->id_jabatan_eselon;
                },
            ],
        ],
    ]) ?>

    </div>
    <div class="box-footer with-border">
        <p>
            <?= Html::a('<i class="fa fa-pencil"></i> Sunting', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat']) ?>
            <?= Html::a('<i class="fa fa-arrow-left"></i> Kembali', ['index', 'id' => $model->id], ['class' => 'btn btn-warning btn-flat']) ?>
        </p>
    </div>

</div>
