<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Jabatan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-primary jabatan-form">
    <div class="box-header with-border">
        <h3 class="box-title">Form jabatan</h3>
    </div>
    <div class="box-body">
    <?php $form = ActiveForm::begin([
                        'layout'=>'horizontal',
                        'fieldConfig' => [
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-3',
                            'wrapper' => 'col-sm-4',
                            'error' => '',
                            'hint' => '',
                    ],
                    ]]); ?>

    <?= $form->field($model, 'id_induk')->widget(Select2::className(), [
        'data' => $model->getListInduk(),
        'options' => [
            'placeholder' => '- Pilih Induk Jabatan -',
        ],
        'pluginOptions' => [
            'allowClear' => true,
        ]
    ]) ?>

    <?= $form->field($model, 'id_jabatan_jenis')->widget(Select2::className(), [
        'data' => $model->getListJabatanJenis(),
        'options' => [
            'placeholder' => '- Pilih Jenis Jabatan -',
        ],
        'pluginOptions' => [
            'allowClear' => true,
        ]
    ]) ?>

    <?= $form->field($model, 'id_jabatan_eselon')->widget(Select2::className(), [
        'data' => $model->getListJabatanEselon(),
        'options' => [
            'placeholder' => '- Pilih Eselon Jabatan -',
        ],
        'pluginOptions' => [
            'allowClear' => true,
        ]
    ]) ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'singkatan')->textInput(['maxlength' => true]) ?>

    </div>
    <div class="box-footer with-border form-group">
        <div class="col-sm-3 col-sm-offset-3">
            <?= Html::submitButton('Simpan', ['class' => 'btn btn-success btn-flat']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
