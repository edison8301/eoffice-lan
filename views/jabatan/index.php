<?php


use yii\helpers\Html;

$this->title = "Kelola Jabatan";
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">
            Struktur Jabatan
        </h3>
    </div>
    <div class="box-body">
        <table class="table table-striped table-hover table-condensed">
            <thead>
            <tr>
                <th style="text-align: left;">
                    Jabatan
                </th>
                <th>
                    Jenis
                </th>
                <th>
                    Eselon
                </th>
                <th>
                    Nama Pemangku
                </th>
                <th>
                    Jumlah
                </th>
            </tr>
            </thead>
            <tbody>
                <?php if (is_array($atasan)) {
                    foreach ($atasan as $jabatan) {
                        echo $this->render('_tabelJabatan', ['jabatan' => $jabatan, 'level' => 0]);
                    }
                } else {
                    echo $this->render('_tabelJabatan', ['jabatan' => $atasan, 'level' => 0]);
                } ?>
            </tbody>
        </table>
    </div>
</div>
