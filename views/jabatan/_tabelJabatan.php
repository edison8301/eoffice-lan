<?php

use yii\helpers\Html;
use yii\bootstrap\ButtonDropdown;
use app\widgets\Label;
?>
<tr>
    <td style="padding-left:<?php print $level*35; ?>px">
        <?= ButtonDropdown::widget([
            'label' => null,
            'dropdown' => [
                'encodeLabels' => false,
                'items' => [
                    ['label' => '<i class="fa fa-arrow-up"></i> Atasan', 'url' => $jabatan->induk !== null ? ['update', 'id' => $jabatan->induk->id] : '#'],
                    '<li role="presentation" class="divider"></li>',
                    ['label' => '<i class="fa fa-pencil"></i> Sunting', 'url' => ['update', 'id' => $jabatan->id]],
                    ['label' => '<i class="fa fa-plus"></i> Subjabatan', 'url' => ['create', 'id_induk' => $jabatan->id]],
                ],
            ],
            'options' => [
                'class' => 'btn-flat btn-primary btn-xs',
            ]
        ]); ?>
        <?= Html::a($jabatan->nama, ['index', 'id' => $jabatan->id]); ?>
        <?= !empty($jabatan->singkatan) ? '(' . $jabatan->singkatan . ')' : null; ?>
    </td>
    <td style="text-align:center"><?php print $jabatan->jabatanJenis->nama; ?></td>
    <td style="text-align:center"><?php print $jabatan->getEselon(); ?></td>
    <td>
        <ul id="pemangku">
            <?php foreach ($jabatan->pegawais as $pegawai): ?>
                <li>
                    <?= Html::a($pegawai->nama, ['pegawai/view', 'id' => $pegawai->id]); ?>
                </li>
            <?php endforeach ?>
        </ul>
    </td>
    <td style="text-align:center">
        <?= Label::widget([
            'text' => $jabatan->countPegawai(),
            'context' => 'info',
        ]); ?>
    </td>
</tr>
<?php $level++; ?>
<?php foreach($jabatan->subJabatan as $subjabatan) { ?>
    <?= $this->render('_tabelJabatan',array('jabatan'=>$subjabatan,'level'=>$level)); ?>
<?php } ?>
