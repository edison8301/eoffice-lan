<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\JabatanEselon */

$this->title = 'Tambah Jabatan Eselon';
$this->params['breadcrumbs'][] = ['label' => 'Jabatan Eselon', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jabatan-eselon-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
