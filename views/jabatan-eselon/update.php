<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\JabatanEselon */

$this->title = 'Sunting Jabatan Eselon';
$this->params['breadcrumbs'][] = ['label' => 'Jabatan Eselon', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Sunting';
?>
<div class="jabatan-eselon-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
