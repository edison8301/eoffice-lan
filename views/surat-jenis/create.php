<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SuratJenis */

$this->title = 'Tambah Surat Jenis';
$this->params['breadcrumbs'][] = ['label' => 'Surat Jenis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surat-jenis-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
