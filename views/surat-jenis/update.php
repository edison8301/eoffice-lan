<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SuratJenis */

$this->title = 'Sunting Surat Jenis';
$this->params['breadcrumbs'][] = ['label' => 'Surat Jenis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Sunting';
?>
<div class="surat-jenis-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
