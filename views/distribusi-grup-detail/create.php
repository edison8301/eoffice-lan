<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DistribusiGrupDetail */

$this->title = 'Tambah Distribusi Grup Detail';
$this->params['breadcrumbs'][] = ['label' => 'Distribusi Grup Detail', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="distribusi-grup-detail-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
