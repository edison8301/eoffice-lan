<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DistribusiGrupDetail */

$this->title = 'Sunting Distribusi Grup Detail';
$this->params['breadcrumbs'][] = ['label' => 'Distribusi Grup Detail', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Sunting';
?>
<div class="distribusi-grup-detail-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
