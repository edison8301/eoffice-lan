<?php

use yii\helpers\Html;
use app\components\Helper;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DistribusiGrupDetailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kelola Distribusi Grup Detail';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary distribusi-grup-detail-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="box-header with-border">
        <p>
            <?= Html::a('<i class="fa fa-plus"></i> Tambah Distribusi Grup Detail', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
        </p>
    </div>
    <div class="box-body">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => 'No',
                'headerOptions'=>['style'=>'text-align:center;width:20px;'],
                'contentOptions'=>['style'=>'text-align:center;width:20px;']
            ],

            [
                'attribute'=>'id_distribusi_grup',
                'value'=>function($data) {
                    return $data->id_distribusi_grup;
                },
            ],
            'model',
            [
                'attribute'=>'id_model',
                'value'=>function($data) {
                    return $data->id_model;
                },
            ],

            [
                'class' => 'app\components\ToggleActionColumn',
                'headerOptions'=>['style'=>'text-align:center;width:80px'],
                'contentOptions'=>['style'=>'text-align:center']
            ],
        ],
    ]); ?>
    </div>
</div>
