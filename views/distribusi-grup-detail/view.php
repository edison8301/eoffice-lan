<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\components\Helper;

/* @var $this yii\web\View */
/* @var $model app\models\DistribusiGrupDetail */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Distribusi Grup Detail', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary distribusi-grup-detail-view">
    <div class="box-header with-border">
        <h1 class="box-title">Detail DistribusiGrupDetail <?= Html::encode($this->title) ?></h1>
    </div>
    <div class="box-body">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute'=>'id_distribusi_grup',
                'value'=>function($data) {
                    return $data->id_distribusi_grup;
                },
            ],
            'model',
            [
                'attribute'=>'id_model',
                'value'=>function($data) {
                    return $data->id_model;
                },
            ],
        ],
    ]) ?>

    </div>
    <div class="box-footer with-border">
        <p>
            <?= Html::a('<i class="fa fa-pencil"></i> Sunting', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat']) ?>
            <?= Html::a('<i class="fa fa-arrow-left"></i> Kembali', ['index', 'id' => $model->id], ['class' => 'btn btn-warning btn-flat']) ?>
        </p>
    </div>

</div>
