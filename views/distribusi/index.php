<?php

use yii\helpers\Html;
use app\components\Helper;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DistribusiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kelola Distribusi';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary distribusi-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="box-header with-border">
        <p>
            <?= Html::a('<i class="fa fa-plus"></i> Tambah Distribusi', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
        </p>
    </div>
    <div class="box-body">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => 'No',
                'headerOptions'=>['style'=>'text-align:center;width:20px;'],
                'contentOptions'=>['style'=>'text-align:center;width:20px;']
            ],

            [
                'attribute'=>'id_surat',
                'value'=>function($data) {
                    return $data->id_surat;
                },
            ],
            [
                'attribute'=>'id_distribusi_jenis',
                'value'=>function($data) {
                    return $data->id_distribusi_jenis;
                },
            ],
            [
                'attribute'=>'id_jabatan_pengirim',
                'value'=>function($data) {
                    return $data->id_jabatan_pengirim;
                },
            ],
            [
                'attribute'=>'id_jabatan_penerima',
                'value'=>function($data) {
                    return $data->id_jabatan_penerima;
                },
            ],
            // 'pengirim',
            // 'username_pengirim',
            // 'username_penerima',
            // 'catatan:ntext',
            // 'tanda',
            // 'waktu_dilihat',
            // 'waktu_dibuat',

            [
                'class' => 'app\components\ToggleActionColumn',
                'headerOptions'=>['style'=>'text-align:center;width:80px'],
                'contentOptions'=>['style'=>'text-align:center']
            ],
        ],
    ]); ?>
    </div>
</div>
