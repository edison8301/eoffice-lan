<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\components\Helper;

/* @var $this yii\web\View */
/* @var $model app\models\Distribusi */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Distribusi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary distribusi-view">
    <div class="box-header with-border">
        <h1 class="box-title">Detail Distribusi <?= Html::encode($this->title) ?></h1>
    </div>
    <div class="box-body">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute'=>'id_surat',
                'value'=>function($data) {
                    return $data->id_surat;
                },
            ],
            [
                'attribute'=>'id_distribusi_jenis',
                'value'=>function($data) {
                    return $data->id_distribusi_jenis;
                },
            ],
            [
                'attribute'=>'id_jabatan_pengirim',
                'value'=>function($data) {
                    return $data->id_jabatan_pengirim;
                },
            ],
            [
                'attribute'=>'id_jabatan_penerima',
                'value'=>function($data) {
                    return $data->id_jabatan_penerima;
                },
            ],
            'pengirim',
            'username_pengirim',
            'username_penerima',
            'catatan:ntext',
            'tanda',
            'waktu_dilihat',
            'waktu_dibuat',
        ],
    ]) ?>

    </div>
    <div class="box-footer with-border">
        <p>
            <?= Html::a('<i class="fa fa-pencil"></i> Sunting', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat']) ?>
            <?= Html::a('<i class="fa fa-arrow-left"></i> Kembali', ['index', 'id' => $model->id], ['class' => 'btn btn-warning btn-flat']) ?>
        </p>
    </div>

</div>
