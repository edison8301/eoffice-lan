<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Distribusi */

$this->title = 'Tambah Distribusi';
$this->params['breadcrumbs'][] = ['label' => 'Distribusi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="distribusi-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
