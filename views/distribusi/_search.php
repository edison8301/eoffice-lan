<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DistribusiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="distribusi-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_surat') ?>

    <?= $form->field($model, 'id_distribusi_jenis') ?>

    <?= $form->field($model, 'id_jabatan_pengirim') ?>

    <?= $form->field($model, 'id_jabatan_penerima') ?>

    <?php // echo $form->field($model, 'pengirim') ?>

    <?php // echo $form->field($model, 'username_pengirim') ?>

    <?php // echo $form->field($model, 'username_penerima') ?>

    <?php // echo $form->field($model, 'catatan') ?>

    <?php // echo $form->field($model, 'tanda') ?>

    <?php // echo $form->field($model, 'waktu_dilihat') ?>

    <?php // echo $form->field($model, 'waktu_dibuat') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
