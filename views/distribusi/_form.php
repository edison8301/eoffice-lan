<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Distribusi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-primary distribusi-form">
    <div class="box-header with-border">
        <h3 class="box-title">Form distribusi</h3>
    </div>
    <div class="box-body">
    <?php $form = ActiveForm::begin([            
                        'layout'=>'horizontal',
                        'fieldConfig' => [
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-3',
                            'wrapper' => 'col-sm-4',
                            'error' => '',
                            'hint' => '',
                    ],
                    ]]); ?>

    <?= $form->field($model, 'id_surat')->textInput() ?>

    <?= $form->field($model, 'id_distribusi_jenis')->textInput() ?>

    <?= $form->field($model, 'id_jabatan_pengirim')->textInput() ?>

    <?= $form->field($model, 'id_jabatan_penerima')->textInput() ?>

    <?= $form->field($model, 'pengirim')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'username_pengirim')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'username_penerima')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'catatan')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'tanda')->textInput() ?>

    <?= $form->field($model, 'waktu_dilihat')->textInput() ?>

    <?= $form->field($model, 'waktu_dibuat')->textInput() ?>

    </div>
    <div class="box-footer with-border form-group">
        <div class="col-sm-3 col-sm-offset-3">
            <?= Html::submitButton('Simpan', ['class' => 'btn btn-success btn-flat']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
