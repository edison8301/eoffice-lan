<?php

use yii\helpers\Html;
use app\components\Helper;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kelola Paraf';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary paraf-index">

    <div class="box-header with-border">
        <p>
            <?= Html::a('<i class="fa fa-plus"></i> Tambah Paraf', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
        </p>
    </div>
    <div class="box-body">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => 'No',
                'headerOptions'=>['style'=>'text-align:center;width:20px;'],
                'contentOptions'=>['style'=>'text-align:center;width:20px;']
            ],

            [
                'attribute'=>'id_surat',
                'value'=>function($data) {
                    return $data->id_surat;
                },
            ],
            [
                'attribute'=>'id_jabatan',
                'value'=>function($data) {
                    return $data->id_jabatan;
                },
            ],
            [
                'attribute'=>'id_paraf_status',
                'value'=>function($data) {
                    return $data->id_paraf_status;
                },
            ],
            'username_pemaraf',
            // 'urutan',
            // 'urutan_tampil',
            // 'keterangan:ntext',
            // 'waktu_dilihat',
            // 'waktu_disetujui',

            [
                'class' => 'app\components\ToggleActionColumn',
                'headerOptions'=>['style'=>'text-align:center;width:80px'],
                'contentOptions'=>['style'=>'text-align:center']
            ],
        ],
    ]); ?>
    </div>
</div>
