<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\components\Helper;

/* @var $this yii\web\View */
/* @var $model app\models\Paraf */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Paraf', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary paraf-view">
    <div class="box-header with-border">
        <h1 class="box-title">Detail Paraf <?= Html::encode($this->title) ?></h1>
    </div>
    <div class="box-body">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute'=>'id_surat',
                'value'=>function($data) {
                    return $data->id_surat;
                },
            ],
            [
                'attribute'=>'id_jabatan',
                'value'=>function($data) {
                    return $data->id_jabatan;
                },
            ],
            [
                'attribute'=>'id_paraf_status',
                'value'=>function($data) {
                    return $data->id_paraf_status;
                },
            ],
            'username_pemaraf',
            'urutan',
            'urutan_tampil',
            'keterangan:ntext',
            'waktu_dilihat',
            'waktu_disetujui',
        ],
    ]) ?>

    </div>
    <div class="box-footer with-border">
        <p>
            <?= Html::a('<i class="fa fa-pencil"></i> Sunting', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat']) ?>
            <?= Html::a('<i class="fa fa-arrow-left"></i> Kembali', ['index', 'id' => $model->id], ['class' => 'btn btn-warning btn-flat']) ?>
        </p>
    </div>

</div>
