<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Paraf */

$this->title = 'Tambah Paraf';
$this->params['breadcrumbs'][] = ['label' => 'Paraf', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paraf-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
