<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DistribusiJenis */

$this->title = 'Sunting Distribusi Jenis';
$this->params['breadcrumbs'][] = ['label' => 'Distribusi Jenis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Sunting';
?>
<div class="distribusi-jenis-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
