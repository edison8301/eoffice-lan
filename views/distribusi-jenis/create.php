<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DistribusiJenis */

$this->title = 'Tambah Distribusi Jenis';
$this->params['breadcrumbs'][] = ['label' => 'Distribusi Jenis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="distribusi-jenis-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
