<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ParafStatus */

$this->title = 'Sunting Paraf Status';
$this->params['breadcrumbs'][] = ['label' => 'Paraf Status', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Sunting';
?>
<div class="paraf-status-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
