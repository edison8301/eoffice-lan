<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ParafStatus */

$this->title = 'Tambah Paraf Status';
$this->params['breadcrumbs'][] = ['label' => 'Paraf Status', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paraf-status-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
