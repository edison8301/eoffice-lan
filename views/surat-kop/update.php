<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SuratKop */

$this->title = 'Sunting Surat Kop';
$this->params['breadcrumbs'][] = ['label' => 'Surat Kop', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Sunting';
?>
<div class="surat-kop-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
