<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SuratKop */

$this->title = 'Tambah Surat Kop';
$this->params['breadcrumbs'][] = ['label' => 'Surat Kop', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surat-kop-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
