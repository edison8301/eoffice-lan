<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DistribusiGrup */

$this->title = 'Sunting Distribusi Grup';
$this->params['breadcrumbs'][] = ['label' => 'Distribusi Grup', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Sunting';
?>
<div class="distribusi-grup-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
