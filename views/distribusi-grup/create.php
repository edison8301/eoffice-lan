<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DistribusiGrup */

$this->title = 'Tambah Distribusi Grup';
$this->params['breadcrumbs'][] = ['label' => 'Distribusi Grup', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="distribusi-grup-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
