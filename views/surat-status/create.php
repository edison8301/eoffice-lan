<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SuratStatus */

$this->title = 'Tambah Surat Status';
$this->params['breadcrumbs'][] = ['label' => 'Surat Status', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surat-status-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
