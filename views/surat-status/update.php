<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SuratStatus */

$this->title = 'Sunting Surat Status';
$this->params['breadcrumbs'][] = ['label' => 'Surat Status', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Sunting';
?>
<div class="surat-status-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
