<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\JabatanJenis */

$this->title = 'Tambah Jabatan Jenis';
$this->params['breadcrumbs'][] = ['label' => 'Jabatan Jenis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jabatan-jenis-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
