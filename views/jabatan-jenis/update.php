<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\JabatanJenis */

$this->title = 'Sunting Jabatan Jenis';
$this->params['breadcrumbs'][] = ['label' => 'Jabatan Jenis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Sunting';
?>
<div class="jabatan-jenis-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
