<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DistribusiAtribut */

$this->title = 'Sunting Distribusi Atribut';
$this->params['breadcrumbs'][] = ['label' => 'Distribusi Atribut', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Sunting';
?>
<div class="distribusi-atribut-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
