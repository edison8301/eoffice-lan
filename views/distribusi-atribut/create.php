<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DistribusiAtribut */

$this->title = 'Tambah Distribusi Atribut';
$this->params['breadcrumbs'][] = ['label' => 'Distribusi Atribut', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="distribusi-atribut-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
