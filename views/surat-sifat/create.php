<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SuratSifat */

$this->title = 'Tambah Surat Sifat';
$this->params['breadcrumbs'][] = ['label' => 'Surat Sifat', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surat-sifat-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
