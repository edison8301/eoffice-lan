<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\components\Helper;

/* @var $this yii\web\View */
/* @var $model app\models\Pegawai */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Pegawai', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary pegawai-view">
    <div class="box-header with-border">
        <h1 class="box-title">Detail Pegawai <?= Html::encode($this->title) ?></h1>
    </div>
    <div class="box-body">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nama',
            'nip',
            'status_struktural',
            [
                'attribute'=>'id_jabatan',
                'value'=>function($data) {
                    return $data->id_jabatan;
                },
            ],
            'email:email',
            'login_terakhir',
        ],
    ]) ?>

    </div>
    <div class="box-footer with-border">
        <p>
            <?= Html::a('<i class="fa fa-pencil"></i> Sunting', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat']) ?>
            <?= Html::a('<i class="fa fa-arrow-left"></i> Kembali', ['index', 'id' => $model->id], ['class' => 'btn btn-warning btn-flat']) ?>
        </p>
    </div>

</div>
