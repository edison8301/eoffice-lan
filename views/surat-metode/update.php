<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SuratMetode */

$this->title = 'Sunting Surat Metode';
$this->params['breadcrumbs'][] = ['label' => 'Surat Metode', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Sunting';
?>
<div class="surat-metode-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
