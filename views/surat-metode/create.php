<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SuratMetode */

$this->title = 'Tambah Surat Metode';
$this->params['breadcrumbs'][] = ['label' => 'Surat Metode', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surat-metode-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
