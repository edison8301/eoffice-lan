<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Surat */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-primary surat-form">
    <div class="box-header with-border">
        <h3 class="box-title">Form surat</h3>
    </div>
    <div class="box-body">
    <?php $form = ActiveForm::begin([
                        'layout'=>'horizontal',
                        'fieldConfig' => [
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-3',
                            'wrapper' => 'col-sm-4',
                            'error' => '',
                            'hint' => '',
                    ],
                    ]]); ?>

    <?= $form->field($model, 'id_surat_jenis')->widget(Select2::className(), [
        'data' => $model->getListSuratJenis(),
        'options' => [
            'placeholder' => '- Pilih Jenis -',
        ],
        'pluginOptions' => [
            'allowClear' => true,
        ]
    ]) ?>

    <?= $form->field($model, 'id_surat_sifat')->widget(Select2::className(), [
        'data' => $model->getListSuratSifat(),
        'options' => [
            'placeholder' => '- Pilih Sifat -',
        ],
        'pluginOptions' => [
            'allowClear' => true,
        ]
    ]) ?>

    <?= $form->field($model, 'id_jabatan_penandatangan')->widget(Select2::className(), [
        'data' => $model->getListJabatanPendandatangan(),
        'options' => [
            'placeholder' => '- Pilih Penandatangan -',
        ],
        'pluginOptions' => [
            'allowClear' => true,
        ]
    ]) ?>

    <?= $form->field($model, 'ringkasan')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'username_pengaju')->widget(Select2::className(), [
        'data' => $model->getListUserPegawai(),
        'options' => [
            'placeholder' => 'Sama dengan pembuat',
        ],
        'pluginOptions' => [
            'allowClear' => true,
        ]
    ]) ?>

    <?= $form->field($model, 'paraf')->widget(Select2::className(), [
        'data' => $model->getListParaf(),
    ]) ?>

    </div>
    <div class="box-footer with-border form-group">
        <div class="col-sm-3 col-sm-offset-3">
            <?= Html::submitButton('Simpan', ['class' => 'btn btn-success btn-flat']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
