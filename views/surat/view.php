<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use app\components\Helper;
use app\widgets\Label;

/* @var $this yii\web\View */
/* @var $model app\models\Surat */

$this->title = $model->ringkasan;
$this->params['breadcrumbs'][] = ['label' => 'Surat', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary surat-view">
    <div class="box-header with-border">
        <h1 class="box-title">Detail Surat <?= Html::encode($this->title) ?></h1>
    </div>
    <div class="box-body">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nomor',
            'tanggal',
            [
                'attribute' => 'id_surat_jenis',
                'value' => $model->suratJenis->nama,
            ],
            [
                'attribute' => 'id_surat_sifat',
                'value' => $model->suratSifat->nama,
            ],
            [
                'attribute' => 'id_jabatan_penandatangan',
                'value' => $model->jabatanPenandatangan->nama,
            ],
            'ringkasan',
            [
                'attribute' => 'username_pengaju',
                'value' => $model->jabatanPengaju !== null ? $model->jabatanPengaju->nama : null,
            ],
            [
                'attribute' => 'username_pembuat',
                'value' => $model->jabatanPembuat !== null ? $model->jabatanPembuat->nama : null,
            ],
            [
                'attribute' => 'id_surat_status',
                'format' => 'raw',
                'value' => function($data) {
                    return Label::widget([
                        'text' => $data->suratStatus->nama,
                        'context' => 'primary',
                    ]);
                }
            ]
        ],
    ]) ?>

    </div>
    <div class="box-footer with-border">
        <p>
            <?= Html::a('<i class="fa fa-pencil"></i> Sunting', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat']) ?>
            <?= Html::a('<i class="fa fa-arrow-left"></i> Kembali', ['index', 'id' => $model->id], ['class' => 'btn btn-warning btn-flat']) ?>
        </p>
    </div>

</div>

<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title">
            <i class="fa fa-print"></i> Pratinjau Surat : <?= Label::widget(['text' => 'Metode', 'context' => 'info']) ?>
        </h3>
    </div>
    <div class="panel-body">
        <iframe width="100%" height="500px" frameborder="0" src="<?= Url::to(['surat/pdf', 'id' => $model->id]) ?>">
        </iframe>
    </div>
</div>
