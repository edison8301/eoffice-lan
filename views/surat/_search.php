<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SuratSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="surat-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_surat_kop') ?>

    <?= $form->field($model, 'id_surat_jenis') ?>

    <?= $form->field($model, 'id_surat_sifat') ?>

    <?= $form->field($model, 'id_surat_status') ?>

    <?php // echo $form->field($model, 'id_surat_metode') ?>

    <?php // echo $form->field($model, 'id_jabatan_penandatangan') ?>

    <?php // echo $form->field($model, 'id_jabatan_penerbit') ?>

    <?php // echo $form->field($model, 'id_jabatan_pembuat') ?>

    <?php // echo $form->field($model, 'id_jabatan_pengaju') ?>

    <?php // echo $form->field($model, 'username_pengaju') ?>

    <?php // echo $form->field($model, 'username_pembuat') ?>

    <?php // echo $form->field($model, 'username_penerbit') ?>

    <?php // echo $form->field($model, 'username_penandatangan') ?>

    <?php // echo $form->field($model, 'nomor') ?>

    <?php // echo $form->field($model, 'urutan') ?>

    <?php // echo $form->field($model, 'tanggal') ?>

    <?php // echo $form->field($model, 'ringkasan') ?>

    <?php // echo $form->field($model, 'draf') ?>

    <?php // echo $form->field($model, 'berkas') ?>

    <?php // echo $form->field($model, 'lampiran') ?>

    <?php // echo $form->field($model, 'tujuan') ?>

    <?php // echo $form->field($model, 'tujuan_jabatan') ?>

    <?php // echo $form->field($model, 'tujuan_pegawai') ?>

    <?php // echo $form->field($model, 'tembusan') ?>

    <?php // echo $form->field($model, 'tembusan_jabatan') ?>

    <?php // echo $form->field($model, 'tembusan_laporan') ?>

    <?php // echo $form->field($model, 'tembusan_pegawai') ?>

    <?php // echo $form->field($model, 'waktu_dibuat') ?>

    <?php // echo $form->field($model, 'waktu_diajukan') ?>

    <?php // echo $form->field($model, 'waktu_disetujui') ?>

    <?php // echo $form->field($model, 'waktu_diterbitkan') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
