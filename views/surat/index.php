<?php

use yii\helpers\Html;
use app\components\Helper;
use yii\grid\GridView;
use app\widgets\Label;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SuratSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kelola Surat';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary surat-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="box-header with-border">
        <p>
            <?= Html::a('<i class="fa fa-plus"></i> Tambah Surat', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
        </p>
    </div>
    <div class="box-body">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'nomor',
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => 'No',
                'headerOptions'=>['style'=>'text-align:center;width:20px;'],
                'contentOptions'=>['style'=>'text-align:center;width:20px;']
            ],

            'ringkasan',
            [
                'attribute' => 'id_surat_jenis',
                'value' => function($data) {
                    return $data->suratJenis->nama;
                },
            ],
            [
                'attribute' => 'id_surat_sifat',
                'value' => function($data) {
                    return $data->suratSifat->nama;
                },
            ],
            [
                'attribute' => 'id_surat_status',
                'format' => 'raw',
                'value'=>function($data) {
                    return Label::widget([
                        'text' => $data->suratStatus->nama,
                        'context' => 'primary'
                    ]);
                },
                'headerOptions' => ['style' => 'text-align:center;'],
                'contentOptions' => ['style' => 'text-align:center;']
            ],
            /*[
                'attribute'=>'id_surat_metode',
                'value'=>function($data) {
                    return $data->id_surat_metode;
                },
            ],*/
            /*[
                'attribute'=>'id_jabatan_penandatangan',
                'value'=>function($data) {
                    return $data->id_jabatan_penandatangan;
                },
            ],*/
            /*[
                'attribute'=>'id_jabatan_penerbit',
                'value'=>function($data) {
                    return $data->id_jabatan_penerbit;
                },
            ],*/
            /*[
                'attribute'=>'id_jabatan_pembuat',
                'value'=>function($data) {
                    return $data->id_jabatan_pembuat;
                },
            ],*/
            /*[
                'attribute'=>'id_jabatan_pengaju',
                'value'=>function($data) {
                    return $data->id_jabatan_pengaju;
                },
            ],*/
            // 'username_pengaju',
            // 'username_pembuat',
            // 'username_penerbit',
            // 'username_penandatangan',
            // 'nomor',
            // 'urutan',
            /*[
                'attribute'=>'tanggal',
                'value'=>function($data) {
                    return Helper::getTanggalSingkat($data->tanggal);
                },
            ],*/
            // 'ringkasan:ntext',
            // 'draf',
            // 'berkas',
            // 'lampiran',
            // 'tujuan:ntext',
            // 'tujuan_jabatan:ntext',
            // 'tujuan_pegawai:ntext',
            // 'tembusan:ntext',
            // 'tembusan_jabatan:ntext',
            // 'tembusan_laporan:ntext',
            // 'tembusan_pegawai:ntext',
            // 'waktu_dibuat',
            // 'waktu_diajukan',
            // 'waktu_disetujui',
            // 'waktu_diterbitkan',

            [
                'class' => 'app\components\ToggleActionColumn',
                'headerOptions'=>['style'=>'text-align:center;width:80px'],
                'contentOptions'=>['style'=>'text-align:center']
            ],
        ],
    ]); ?>
    </div>
</div>
