<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "surat_status".
 *
 * @property integer $id
 * @property string $nama
 *
 * @property Surat[] $surats
 */
class SuratStatus extends \yii\db\ActiveRecord
{
    const TERBIT = 1;
    const KONSEP = 2;
    const PEMERIKSAAN = 3;
    const PERBAIKAN = 4;
    const DISETUJUI = 5;
    const DITOLAK = 6;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'surat_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurats()
    {
        return $this->hasMany(Surat::className(), ['id_surat_status' => 'id']);
    }

    public static function getList()
    {
        return ArrayHelper::map(static::find()->all(), 'id', 'nama');
    }
}
