<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "distribusi_jenis".
 *
 * @property integer $id
 * @property string $nama
 *
 * @property Distribusi[] $distribusis
 */
class DistribusiJenis extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'distribusi_jenis';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistribusis()
    {
        return $this->hasMany(Distribusi::className(), ['id_distribusi_jenis' => 'id']);
    }
}
