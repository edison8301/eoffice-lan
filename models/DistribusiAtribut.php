<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "distribusi_atribut".
 *
 * @property integer $id
 * @property integer $id_distribusi
 * @property string $nama
 * @property string $nilai
 *
 * @property Distribusi $idDistribusi
 */
class DistribusiAtribut extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'distribusi_atribut';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_distribusi'], 'integer'],
            [['nilai'], 'string'],
            [['nama'], 'string', 'max' => 255],
            [['id_distribusi'], 'exist', 'skipOnError' => true, 'targetClass' => Distribusi::className(), 'targetAttribute' => ['id_distribusi' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_distribusi' => 'Id Distribusi',
            'nama' => 'Nama',
            'nilai' => 'Nilai',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdDistribusi()
    {
        return $this->hasOne(Distribusi::className(), ['id' => 'id_distribusi']);
    }
}
