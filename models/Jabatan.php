<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "jabatan".
 *
 * @property integer $id
 * @property integer $id_induk
 * @property string $nama
 * @property string $singkatan
 * @property integer $id_jabatan_jenis
 * @property integer $id_jabatan_eselon
 *
 * @property Distribusi[] $distribusis
 * @property Distribusi[] $distribusis0
 * @property JabatanJenis $idJabatanJenis
 * @property JabatanEselon $idJabatanEselon
 * @property Jabatan $idInduk
 * @property Jabatan[] $jabatans
 * @property Paraf[] $parafs
 * @property Pegawai[] $pegawais
 * @property Surat[] $surats
 * @property Surat[] $surats0
 * @property Surat[] $surats1
 * @property Surat[] $surats2
 */
class Jabatan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jabatan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_induk', 'id_jabatan_jenis', 'id_jabatan_eselon'], 'integer'],
            [['nama', 'singkatan'], 'string', 'max' => 255],
            [['id_jabatan_jenis'], 'exist', 'skipOnError' => true, 'targetClass' => JabatanJenis::className(), 'targetAttribute' => ['id_jabatan_jenis' => 'id']],
            [['id_jabatan_eselon'], 'exist', 'skipOnError' => true, 'targetClass' => JabatanEselon::className(), 'targetAttribute' => ['id_jabatan_eselon' => 'id']],
            [['id_induk'], 'exist', 'skipOnError' => true, 'targetClass' => Jabatan::className(), 'targetAttribute' => ['id_induk' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_induk' => 'Induk Jabatan',
            'nama' => 'Nama',
            'singkatan' => 'Singkatan',
            'id_jabatan_jenis' => 'Jenis Jabatan',
            'id_jabatan_eselon' => 'Eselon Jabatan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistribusis()
    {
        return $this->hasMany(Distribusi::className(), ['id_jabatan_pengirim' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistribusis0()
    {
        return $this->hasMany(Distribusi::className(), ['id_jabatan_penerima' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJabatanJenis()
    {
        return $this->hasOne(JabatanJenis::className(), ['id' => 'id_jabatan_jenis']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJabatanEselon()
    {
        return $this->hasOne(JabatanEselon::className(), ['id' => 'id_jabatan_eselon']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInduk()
    {
        return $this->hasOne(Jabatan::className(), ['id' => 'id_induk']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubJabatan()
    {
        return $this->hasMany(Jabatan::className(), ['id_induk' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParafs()
    {
        return $this->hasMany(Paraf::className(), ['id_jabatan' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawais()
    {
        return $this->hasMany(Pegawai::className(), ['id_jabatan' => 'id']);
    }

    /**
     * @return int
     */
    public function countPegawai()
    {
        return $this->getPegawais()->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurats()
    {
        return $this->hasMany(Surat::className(), ['id_jabatan_pengaju' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurats0()
    {
        return $this->hasMany(Surat::className(), ['id_jabatan_penandatangan' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurats1()
    {
        return $this->hasMany(Surat::className(), ['id_jabatan_penerbit' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurats2()
    {
        return $this->hasMany(Surat::className(), ['id_jabatan_pembuat' => 'id']);
    }

    /**
     * Ambil list jabatan kecuali yang memiliki idnya sendiri
     * @return array
     */
    public function getListInduk()
    {
        return static::getList($this->id);
    }

    /**
     * @return array
     */
    public static function getList($id = null)
    {
        return ArrayHelper::map(static::find()->andFilterWhere(['<>', 'id', $id])->all(), 'id', 'nama');
    }

    /**
     * @return array
     */
    public function getListJabatanJenis()
    {
        return JabatanJenis::getList();
    }

    /**
     * @return array
     */
    public function getListJabatanEselon()
    {
        return JabatanEselon::getList();
    }

    public static function findAllAtasan()
    {
        return static::find()->where(['id_induk' => null])->all();
    }

    public function getEselon()
    {
        if ($this->jabatanEselon !== null) {
            return $this->jabatanEselon->nama;
        } else {
            return null;
        }
    }

    public function getSingkatan()
    {
        if ($this->singkatan !== null) {
            return $this->singkatan;
        } else {
            return $this->nama;
        }
    }

}
