<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "surat_jenis".
 *
 * @property integer $id
 * @property string $nama
 * @property string $penandatangan
 * @property string $contoh
 * @property string $keterangan
 *
 * @property Surat[] $surats
 */
class SuratJenis extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'surat_jenis';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'required'],
            [['keterangan'], 'string'],
            [['nama', 'penandatangan', 'contoh'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'penandatangan' => 'Penandatangan',
            'contoh' => 'Contoh',
            'keterangan' => 'Keterangan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSurats()
    {
        return $this->hasMany(Surat::className(), ['id_surat_jenis' => 'id']);
    }

    public static function getList()
    {
        return ArrayHelper::map(static::find()->all(), 'id', 'nama');
    }
}
