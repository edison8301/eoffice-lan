<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Distribusi;

/**
 * DistribusiSearch represents the model behind the search form of `app\models\Distribusi`.
 */
class DistribusiSearch extends Distribusi
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_surat', 'id_distribusi_jenis', 'id_jabatan_pengirim', 'id_jabatan_penerima', 'tanda'], 'integer'],
            [['pengirim', 'username_pengirim', 'username_penerima', 'catatan', 'waktu_dilihat', 'waktu_dibuat'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

    public function getQuerySearch($params)
    {
        $query = Distribusi::find();

        $this->load($params);

        // add conditions that should always apply here

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_surat' => $this->id_surat,
            'id_distribusi_jenis' => $this->id_distribusi_jenis,
            'id_jabatan_pengirim' => $this->id_jabatan_pengirim,
            'id_jabatan_penerima' => $this->id_jabatan_penerima,
            'tanda' => $this->tanda,
            'waktu_dilihat' => $this->waktu_dilihat,
            'waktu_dibuat' => $this->waktu_dibuat,
        ]);

        $query->andFilterWhere(['like', 'pengirim', $this->pengirim])
            ->andFilterWhere(['like', 'username_pengirim', $this->username_pengirim])
            ->andFilterWhere(['like', 'username_penerima', $this->username_penerima])
            ->andFilterWhere(['like', 'catatan', $this->catatan]);

        return $query;
    }
    
    public function search($params)
    {
        $query = $this->getQuerySearch($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);        

        return $dataProvider;
    }
}
