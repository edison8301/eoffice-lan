<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "distribusi_grup_detail".
 *
 * @property integer $id
 * @property integer $id_distribusi_grup
 * @property string $model
 * @property integer $id_model
 *
 * @property DistribusiGrup $idDistribusiGrup
 */
class DistribusiGrupDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'distribusi_grup_detail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_distribusi_grup', 'id_model'], 'integer'],
            [['model'], 'string', 'max' => 255],
            [['id_distribusi_grup'], 'exist', 'skipOnError' => true, 'targetClass' => DistribusiGrup::className(), 'targetAttribute' => ['id_distribusi_grup' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_distribusi_grup' => 'Id Distribusi Grup',
            'model' => 'Model',
            'id_model' => 'Id Model',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdDistribusiGrup()
    {
        return $this->hasOne(DistribusiGrup::className(), ['id' => 'id_distribusi_grup']);
    }
}
