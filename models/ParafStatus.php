<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "paraf_status".
 *
 * @property integer $id
 * @property string $nama
 *
 * @property Paraf[] $parafs
 */
class ParafStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'paraf_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParafs()
    {
        return $this->hasMany(Paraf::className(), ['id_paraf_status' => 'id']);
    }
}
