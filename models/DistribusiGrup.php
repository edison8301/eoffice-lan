<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "distribusi_grup".
 *
 * @property integer $id
 * @property string $nama
 *
 * @property DistribusiGrupDetail[] $distribusiGrupDetails
 */
class DistribusiGrup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'distribusi_grup';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistribusiGrupDetails()
    {
        return $this->hasMany(DistribusiGrupDetail::className(), ['id_distribusi_grup' => 'id']);
    }
}
