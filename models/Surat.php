<?php

namespace app\models;

use Yii;
use app\components\Helper;

/**
 * This is the model class for table "surat".
 *
 * @property integer $id
 * @property integer $id_surat_kop
 * @property integer $id_surat_jenis
 * @property integer $id_surat_sifat
 * @property integer $id_surat_status
 * @property integer $id_surat_metode
 * @property integer $id_jabatan_penandatangan
 * @property integer $id_jabatan_penerbit
 * @property string $username_pengaju
 * @property string $username_pembuat
 * @property string $username_penerbit
 * @property string $username_penandatangan
 * @property string $nomor
 * @property integer $urutan
 * @property string $tanggal
 * @property string $ringkasan
 * @property string $draf
 * @property string $berkas
 * @property string $lampiran
 * @property string $tujuan
 * @property string $tujuan_jabatan
 * @property string $tujuan_pegawai
 * @property string $tembusan
 * @property string $tembusan_jabatan
 * @property string $tembusan_laporan
 * @property string $tembusan_pegawai
 * @property string $waktu_dibuat
 * @property string $waktu_diajukan
 * @property string $waktu_disetujui
 * @property string $waktu_diterbitkan
 *
 * @property Distribusi[] $distribusis
 * @property Paraf[] $parafs
 * @property Jabatan $idJabatanPengaju
 * @property SuratJenis $idSuratJenis
 * @property SuratSifat $idSuratSifat
 * @property SuratStatus $idSuratStatus
 * @property SuratKop $idSuratKop
 * @property SuratMetode $idSuratMetode
 * @property Jabatan $idJabatanPenandatangan
 * @property Jabatan $idJabatanPenerbit
 * @property Jabatan $idJabatanPembuat
 * @property SuratAtribut[] $suratAtributs
 */
class Surat extends \yii\db\ActiveRecord
{
    public $paraf;

    const EVENT_AFTER_CREATE_DRAFT = 'after_create_draft';

    public function init()
    {
        $this->on(self::EVENT_AFTER_CREATE_DRAFT, [$this, 'setTanggal']);
        $this->on(self::EVENT_AFTER_CREATE_DRAFT, [$this, 'setNomor']);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'surat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_surat_jenis', 'id_surat_sifat', 'id_jabatan_penandatangan', 'ringkasan', 'paraf'], 'required'],
            [['paraf','id_surat_kop', 'id_surat_jenis', 'id_surat_sifat', 'id_surat_status', 'id_surat_metode', 'id_jabatan_penandatangan', 'id_jabatan_penerbit', 'urutan'], 'integer'],
            [['tanggal', 'waktu_dibuat', 'waktu_diajukan', 'waktu_disetujui', 'waktu_diterbitkan'], 'safe'],
            [['ringkasan', 'tujuan', 'tujuan_jabatan', 'tujuan_pegawai', 'tembusan', 'tembusan_jabatan', 'tembusan_laporan', 'tembusan_pegawai'], 'string'],
            [['username_pengaju', 'username_pembuat', 'username_penerbit', 'username_penandatangan', 'nomor', 'draf', 'berkas', 'lampiran'], 'string', 'max' => 255],

            [['id_jabatan_penandatangan'], 'exist', 'skipOnError' => true, 'targetClass' => Jabatan::className(), 'targetAttribute' => ['id_jabatan_penandatangan' => 'id']],
            [['id_jabatan_penerbit'], 'exist', 'skipOnError' => true, 'targetClass' => Jabatan::className(), 'targetAttribute' => ['id_jabatan_penerbit' => 'id']],

            [['username_pengaju'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['username_pengaju' => 'username']],
            [['username_penandatangan'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['username_penandatangan' => 'username']],
            [['username_penerbit'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['username_penerbit' => 'username']],
            [['username_pembuat'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['username_pembuat' => 'username']],

            [['id_surat_jenis'], 'exist', 'skipOnError' => true, 'targetClass' => SuratJenis::className(), 'targetAttribute' => ['id_surat_jenis' => 'id']],
            [['id_surat_sifat'], 'exist', 'skipOnError' => true, 'targetClass' => SuratSifat::className(), 'targetAttribute' => ['id_surat_sifat' => 'id']],
            [['id_surat_status'], 'exist', 'skipOnError' => true, 'targetClass' => SuratStatus::className(), 'targetAttribute' => ['id_surat_status' => 'id']],
            [['id_surat_kop'], 'exist', 'skipOnError' => true, 'targetClass' => SuratKop::className(), 'targetAttribute' => ['id_surat_kop' => 'id']],
            [['id_surat_metode'], 'exist', 'skipOnError' => true, 'targetClass' => SuratMetode::className(), 'targetAttribute' => ['id_surat_metode' => 'id']],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_surat_kop' => 'Kop Surat',
            'id_surat_jenis' => 'Jenis Surat',
            'id_surat_sifat' => 'Sifat Surat',
            'id_surat_status' => 'Status Surat',
            'id_surat_metode' => 'Metode Surat',
            'id_jabatan_penandatangan' => 'Jabatan Penandatangan',
            'id_jabatan_penerbit' => 'Jabatan Penerbit',
            'username_pengaju' => 'Username Pengaju',
            'username_pembuat' => 'Username Pembuat',
            'username_penerbit' => 'Username Penerbit',
            'username_penandatangan' => 'Username Penandatangan',
            'nomor' => 'Nomor',
            'urutan' => 'Urutan',
            'tanggal' => 'Tanggal',
            'ringkasan' => 'Ringkasan',
            'draf' => 'Draf',
            'berkas' => 'Berkas',
            'lampiran' => 'Lampiran',
            'tujuan' => 'Tujuan',
            'tujuan_jabatan' => 'Tujuan Jabatan',
            'tujuan_pegawai' => 'Tujuan Pegawai',
            'tembusan' => 'Tembusan',
            'tembusan_jabatan' => 'Tembusan Jabatan',
            'tembusan_laporan' => 'Tembusan Laporan',
            'tembusan_pegawai' => 'Tembusan Pegawai',
            'waktu_dibuat' => 'Waktu Dibuat',
            'waktu_diajukan' => 'Waktu Diajukan',
            'waktu_disetujui' => 'Waktu Disetujui',
            'waktu_diterbitkan' => 'Waktu Diterbitkan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistribusis()
    {
        return $this->hasMany(Distribusi::className(), ['id_surat' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParafs()
    {
        return $this->hasMany(Paraf::className(), ['id_surat' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJabatanPenandatangan()
    {
        return $this->hasOne(Jabatan::className(), ['id' => 'id_jabatan_penandatangan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdJabatanPenerbit()
    {
        return $this->hasOne(Jabatan::className(), ['id' => 'id_jabatan_penerbit']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPengaju()
    {
        return $this->hasOne(User::className(), ['username' => 'username_pengaju']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPembuat()
    {
        return $this->hasOne(User::className(), ['username' => 'username_pembuat']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiPengaju()
    {
        return $this->hasOne(Pegawai::className(), ['id' => 'id_pegawai'])
            ->via('userPengaju');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPegawaiPembuat()
    {
        return $this->hasOne(Pegawai::className(), ['id' => 'id_pegawai'])
            ->via('userPembuat');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJabatanPengaju()
    {
        return $this->hasOne(Jabatan::className(), ['id' => 'id_jabatan'])
            ->via('pegawaiPengaju');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJabatanPembuat()
    {
        return $this->hasOne(Jabatan::className(), ['id' => 'id_jabatan'])
            ->via('pegawaiPembuat');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPenerbit()
    {
        return $this->hasOne(User::className(), ['username' => 'username_penerbit']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPenandatangan()
    {
        return $this->hasOne(User::className(), ['username' => 'username_penandatangan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuratJenis()
    {
        return $this->hasOne(SuratJenis::className(), ['id' => 'id_surat_jenis']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuratSifat()
    {
        return $this->hasOne(SuratSifat::className(), ['id' => 'id_surat_sifat']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuratStatus()
    {
        return $this->hasOne(SuratStatus::className(), ['id' => 'id_surat_status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuratKop()
    {
        return $this->hasOne(SuratKop::className(), ['id' => 'id_surat_kop']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSuratMetode()
    {
        return $this->hasOne(SuratMetode::className(), ['id' => 'id_surat_metode']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuratAtributs()
    {
        return $this->hasMany(SuratAtribut::className(), ['id_surat' => 'id']);
    }

    public function getListSuratKop()
    {
        return SuratKop::getList();
    }

    public function getListSuratJenis()
    {
        return SuratJenis::getList();
    }

    public function getListSuratSifat()
    {
        return SuratSifat::getList();
    }

    public function getListJabatanPendandatangan()
    {
        return Jabatan::getList();
    }

    public function getListParaf()
    {
        return [1 => 'Otomatis', 2 => 'Manual'];
    }

    public function getListPegawai()
    {
        return Pegawai::getList();
    }

    public function getListUserPegawai()
    {
        return User::getListPegawai();
    }

    public function setTanggal()
    {
        $this->tanggal = date('Y-m-d');

        return true;
    }

    public function setUrutan()
    {
        if ($this->urutan === null) {
            $tahun = Yii::$app->session->get('tahun', date('Y'));

            $this->urutan = static::find()->where([
                'id_jabatan_penandatangan' => $this->id_jabatan_penandatangan,
                'id_surat_jenis' => $this->id_surat_jenis
            ])->count() + 1;

            return true;
        } else {
            return false;
        }

    }

    public function setNomor()
    {
        $this->setUrutan();

        $urutan = $this->urutan;

        while (strlen($urutan) < 4) {
            $urutan = '0' . $urutan;
        }

        if ($this->jabatanPenandatangan->id_jabatan_eselon == 1) {
            $belakang = "/" .
                        $this->jabatanPenandatangan->getSingkatan() .
                        "/Bakamla/" .
                        Helper::konversiRomawi(date('n')) .
                        "/" .
                        Yii::$app->session->get('tahun', date('Y'));
        } else {
            $belakang = "/" .
                        $this->jabatanPenandatangan->getSingkatan() .
                        "/Bakamlas/" .
                        Helper::konversiRomawi(date('n')) .
                        "/" .
                        Yii::$app->session->get('tahun', date('Y'));
        }

        $depan = $this->getSingkatanSuratJenis();

        $this->nomor = $depan . $urutan . $belakang;

        $this->save();
    }

    public function getSingkatanSuratJenis()
    {
        switch ($this->id_surat_jenis) {
            case 1:
                return "Skep-";
                break;
            case 2:
                return "Sprin-";
                break;
            case 3:
                return "ST-";
                break;
            case 4:
                return "SD-";
                break;
            case 5:
                return "SPLN-";
                break;
            case 6:
                return "SE-";
                break;
            case 7:
                return "SI-";
                break;
            case 8:
                return "Sket-";
                break;
            case 9:
                return "SKJ-";
                break;
            case 10:
                return "SK-";
                break;
            case 11:
                return "Sper-";
                break;
            case 12:
                return "SC-";
                break;
            case 13:
                return "Un-";
                break;
            case 14:
                return "Skdt-";
                break;
            case 15:
                return "SKPSW-";
                break;
            case 16:
                return "SKTM-";
                break;
            case 17:
                return "M-";
                break;
            case 18:
                return "AGNO-";
                break;
            case 19:
                return "M-";
                break;
            case 20:
                return "M-";
                break;
        }

        return null;
    }
}
