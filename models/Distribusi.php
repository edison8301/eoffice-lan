<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "distribusi".
 *
 * @property integer $id
 * @property integer $id_surat
 * @property integer $id_distribusi_jenis
 * @property integer $id_jabatan_pengirim
 * @property integer $id_jabatan_penerima
 * @property string $pengirim
 * @property string $username_pengirim
 * @property string $username_penerima
 * @property string $catatan
 * @property integer $tanda
 * @property string $waktu_dilihat
 * @property string $waktu_dibuat
 *
 * @property Surat $idSurat
 * @property DistribusiJenis $idDistribusiJenis
 * @property Jabatan $idJabatanPengirim
 * @property Jabatan $idJabatanPenerima
 * @property DistribusiAtribut[] $distribusiAtributs
 */
class Distribusi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'distribusi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_surat', 'id_distribusi_jenis', 'id_jabatan_pengirim', 'id_jabatan_penerima', 'tanda'], 'integer'],
            [['catatan'], 'string'],
            [['waktu_dilihat', 'waktu_dibuat'], 'safe'],
            [['pengirim', 'username_pengirim', 'username_penerima'], 'string', 'max' => 255],
            [['id_surat'], 'exist', 'skipOnError' => true, 'targetClass' => Surat::className(), 'targetAttribute' => ['id_surat' => 'id']],
            [['id_distribusi_jenis'], 'exist', 'skipOnError' => true, 'targetClass' => DistribusiJenis::className(), 'targetAttribute' => ['id_distribusi_jenis' => 'id']],
            [['id_jabatan_pengirim'], 'exist', 'skipOnError' => true, 'targetClass' => Jabatan::className(), 'targetAttribute' => ['id_jabatan_pengirim' => 'id']],
            [['id_jabatan_penerima'], 'exist', 'skipOnError' => true, 'targetClass' => Jabatan::className(), 'targetAttribute' => ['id_jabatan_penerima' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_surat' => 'Id Surat',
            'id_distribusi_jenis' => 'Id Distribusi Jenis',
            'id_jabatan_pengirim' => 'Id Jabatan Pengirim',
            'id_jabatan_penerima' => 'Id Jabatan Penerima',
            'pengirim' => 'Pengirim',
            'username_pengirim' => 'Username Pengirim',
            'username_penerima' => 'Username Penerima',
            'catatan' => 'Catatan',
            'tanda' => 'Tanda',
            'waktu_dilihat' => 'Waktu Dilihat',
            'waktu_dibuat' => 'Waktu Dibuat',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSurat()
    {
        return $this->hasOne(Surat::className(), ['id' => 'id_surat']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdDistribusiJenis()
    {
        return $this->hasOne(DistribusiJenis::className(), ['id' => 'id_distribusi_jenis']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdJabatanPengirim()
    {
        return $this->hasOne(Jabatan::className(), ['id' => 'id_jabatan_pengirim']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdJabatanPenerima()
    {
        return $this->hasOne(Jabatan::className(), ['id' => 'id_jabatan_penerima']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistribusiAtributs()
    {
        return $this->hasMany(DistribusiAtribut::className(), ['id_distribusi' => 'id']);
    }
}
