<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "surat_atribut".
 *
 * @property integer $id
 * @property integer $id_surat
 * @property string $nama
 * @property string $nilai
 *
 * @property Surat $idSurat
 */
class SuratAtribut extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'surat_atribut';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_surat'], 'integer'],
            [['nilai'], 'string'],
            [['nama'], 'string', 'max' => 255],
            [['id_surat'], 'exist', 'skipOnError' => true, 'targetClass' => Surat::className(), 'targetAttribute' => ['id_surat' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_surat' => 'Id Surat',
            'nama' => 'Nama',
            'nilai' => 'Nilai',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSurat()
    {
        return $this->hasOne(Surat::className(), ['id' => 'id_surat']);
    }
}
