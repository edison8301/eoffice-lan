<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Surat;

/**
 * SuratSearch represents the model behind the search form of `app\models\Surat`.
 */
class SuratSearch extends Surat
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_surat_kop', 'id_surat_jenis', 'id_surat_sifat', 'id_surat_status', 'id_surat_metode', 'id_jabatan_penandatangan', 'id_jabatan_penerbit', 'urutan'], 'integer'],
            [['username_pengaju', 'username_pembuat', 'username_penerbit', 'username_penandatangan', 'nomor', 'tanggal', 'ringkasan', 'draf', 'berkas', 'lampiran', 'tujuan', 'tujuan_jabatan', 'tujuan_pegawai', 'tembusan', 'tembusan_jabatan', 'tembusan_laporan', 'tembusan_pegawai', 'waktu_dibuat', 'waktu_diajukan', 'waktu_disetujui', 'waktu_diterbitkan'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

    public function getQuerySearch($params)
    {
        $query = Surat::find();

        $this->load($params);

        // add conditions that should always apply here

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_surat_kop' => $this->id_surat_kop,
            'id_surat_jenis' => $this->id_surat_jenis,
            'id_surat_sifat' => $this->id_surat_sifat,
            'id_surat_status' => $this->id_surat_status,
            'id_surat_metode' => $this->id_surat_metode,
            'id_jabatan_penandatangan' => $this->id_jabatan_penandatangan,
            'id_jabatan_penerbit' => $this->id_jabatan_penerbit,
            'urutan' => $this->urutan,
            'tanggal' => $this->tanggal,
            'waktu_dibuat' => $this->waktu_dibuat,
            'waktu_diajukan' => $this->waktu_diajukan,
            'waktu_disetujui' => $this->waktu_disetujui,
            'waktu_diterbitkan' => $this->waktu_diterbitkan,
        ]);

        $query->andFilterWhere(['like', 'username_pengaju', $this->username_pengaju])
            ->andFilterWhere(['like', 'username_pembuat', $this->username_pembuat])
            ->andFilterWhere(['like', 'username_penerbit', $this->username_penerbit])
            ->andFilterWhere(['like', 'username_penandatangan', $this->username_penandatangan])
            ->andFilterWhere(['like', 'nomor', $this->nomor])
            ->andFilterWhere(['like', 'ringkasan', $this->ringkasan])
            ->andFilterWhere(['like', 'draf', $this->draf])
            ->andFilterWhere(['like', 'berkas', $this->berkas])
            ->andFilterWhere(['like', 'lampiran', $this->lampiran])
            ->andFilterWhere(['like', 'tujuan', $this->tujuan])
            ->andFilterWhere(['like', 'tujuan_jabatan', $this->tujuan_jabatan])
            ->andFilterWhere(['like', 'tujuan_pegawai', $this->tujuan_pegawai])
            ->andFilterWhere(['like', 'tembusan', $this->tembusan])
            ->andFilterWhere(['like', 'tembusan_jabatan', $this->tembusan_jabatan])
            ->andFilterWhere(['like', 'tembusan_laporan', $this->tembusan_laporan])
            ->andFilterWhere(['like', 'tembusan_pegawai', $this->tembusan_pegawai]);

        return $query;
    }

    public function search($params)
    {
        $query = $this->getQuerySearch($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
}
