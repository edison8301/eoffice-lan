<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "paraf".
 *
 * @property integer $id
 * @property integer $id_surat
 * @property integer $id_jabatan
 * @property integer $id_paraf_status
 * @property string $username_pemaraf
 * @property integer $urutan
 * @property integer $urutan_tampil
 * @property string $keterangan
 * @property string $waktu_dilihat
 * @property string $waktu_disetujui
 *
 * @property Surat $idSurat
 * @property Jabatan $idJabatan
 * @property ParafStatus $idParafStatus
 */
class Paraf extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'paraf';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_surat', 'id_jabatan', 'id_paraf_status', 'urutan', 'urutan_tampil'], 'integer'],
            [['keterangan'], 'string'],
            [['waktu_dilihat', 'waktu_disetujui'], 'safe'],
            [['username_pemaraf'], 'string', 'max' => 255],
            [['id_surat'], 'exist', 'skipOnError' => true, 'targetClass' => Surat::className(), 'targetAttribute' => ['id_surat' => 'id']],
            [['id_jabatan'], 'exist', 'skipOnError' => true, 'targetClass' => Jabatan::className(), 'targetAttribute' => ['id_jabatan' => 'id']],
            [['id_paraf_status'], 'exist', 'skipOnError' => true, 'targetClass' => ParafStatus::className(), 'targetAttribute' => ['id_paraf_status' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_surat' => 'Id Surat',
            'id_jabatan' => 'Id Jabatan',
            'id_paraf_status' => 'Id Paraf Status',
            'username_pemaraf' => 'Username Pemaraf',
            'urutan' => 'Urutan',
            'urutan_tampil' => 'Urutan Tampil',
            'keterangan' => 'Keterangan',
            'waktu_dilihat' => 'Waktu Dilihat',
            'waktu_disetujui' => 'Waktu Disetujui',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSurat()
    {
        return $this->hasOne(Surat::className(), ['id' => 'id_surat']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdJabatan()
    {
        return $this->hasOne(Jabatan::className(), ['id' => 'id_jabatan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdParafStatus()
    {
        return $this->hasOne(ParafStatus::className(), ['id' => 'id_paraf_status']);
    }
}
