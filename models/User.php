<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
* This is the model class for table "user".
*
* @property integer $id
* @property integer $id_pegawai
* @property string $username
* @property string $password
* @property string $auth_key
* @property string $access_token
* @property string $role
* @property string $nama
*
* @property Pegawai $pegawai
*/

class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    const ROLE_ADMIN = '10';
    const ROLE_PEGAWAI = '20';
    /**
    * @inheritdoc
    */

    public static function tableName()
    {
        return 'user';
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['id_pegawai'], 'integer'],
            [['username', 'password'], 'required'],
            [['username', 'password', 'auth_key', 'access_token', 'role', 'nama'], 'string', 'max' => 255],
            [['id_pegawai'], 'exist', 'skipOnError' => true, 'targetClass' => Pegawai::className(), 'targetAttribute' => ['id_pegawai' => 'id']],
       ];
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
           'id' => 'ID',
           'nama' => 'Nama',
           'username' => 'Username',
           'password' => 'Password',
           'status' => 'Status',
           'auth_key' => 'Auth Key',
           'access_token' => 'Access Token',
       ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getPegawai()
    {
        return $this->hasOne(Pegawai::className(), ['id' => 'id_pegawai']);
    }

    public static function findAllPegawai()
    {
        return static::find()->joinWith('pegawai')->andWhere(['role' => self::ROLE_PEGAWAI]);
    }

    public static function getListPegawai()
    {
        return ArrayHelper::map(static::findAllPegawai()->all(), 'username', 'pegawai.nama');
    }
}
