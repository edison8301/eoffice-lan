<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "pegawai".
 *
 * @property integer $id
 * @property string $nama
 * @property string $nip
 * @property integer $status_struktural
 * @property integer $id_jabatan
 * @property string $email
 * @property string $login_terakhir
 *
 * @property Jabatan $idJabatan
 * @property User[] $users
 */
class Pegawai extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pegawai';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_struktural', 'id_jabatan'], 'integer'],
            [['login_terakhir'], 'safe'],
            [['nama', 'nip', 'email'], 'string', 'max' => 255],
            [['id_jabatan'], 'exist', 'skipOnError' => true, 'targetClass' => Jabatan::className(), 'targetAttribute' => ['id_jabatan' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'nip' => 'Nip',
            'status_struktural' => 'Status Struktural',
            'id_jabatan' => 'Jabatan',
            'email' => 'Email',
            'login_terakhir' => 'Login Terakhir',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJabatan()
    {
        return $this->hasOne(Jabatan::className(), ['id' => 'id_jabatan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id_pegawai' => 'id']);
    }

    public static function getList()
    {
        return ArrayHelper::map(static::find()->all(), 'id', 'nama');
    }

    public function getListJabatan()
    {
        return Jabatan::getList();
    }

}
